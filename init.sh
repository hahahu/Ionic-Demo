#!/bin/bash
## 在项目根目录下, 直接运行

# 20160229 白名单
sudo ionic plugin add cordova-plugin-whitelist

# 失败 换下一个 sudo cordova plugin add https://github.com/wildabeast/BarcodeScanner.git
sudo cordova plugin add phonegap-plugin-barcodescanner

# 失败 换下一个 sudo cordova plugin add https://github.com/apache/cordova-plugin-camera.git
sudo cordova plugin add cordova-plugin-camera

## 设备
sudo cordova plugin add cordova-plugin-device

## 网络
sudo cordova plugin add cordova-plugin-network-information

## 文件
sudo cordova plugin add cordova-plugin-file

## 打开文件
sudo cordova plugin add https://github.com/pwlin/cordova-plugin-file-opener2.git

## 上传下载文件
sudo cordova plugin add cordova-plugin-file-transfer

# 失败 没有代替的啊!待处理 sudo cordova plugin add https://github.com/wymsee/cordova-imagePicker.git
sudo cordova plugin add https://github.com/VitaliiBlagodir/cordova-plugin-datepicker.git

## 启动画面
sudo cordova plugin add cordova-plugin-splashscreen

## app 偏好设置
## 没有使用 sudo cordova plugin add https://github.com/chrisekelley/AppPreferences

## 内置浏览器
## 没有使用 sudo cordova plugin add cordova-plugin-inappbrowser

# add by wzh 20160302 本地通知
sudo cordova plugin add https://github.com/katzer/cordova-plugin-local-notifications.git

# add by wzh 20160307 键盘
sudo cordova plugin add  https://github.com/driftyco/ionic-plugins-keyboard.git

# add by wzh 20160307 原生提示
sudo cordova plugin add https://github.com/EddyVerbruggen/Toast-PhoneGap-Plugin.git

# by wzh 20160308 发现 app.js 中已经存在 cordova 的 keyboard处理! cordova.plugins.Keyboard
# 这样不行 sudo cordova plugin remove https://github.com/driftyco/ionic-plugins-keyboard.git
# 这样不行 sudo cordova plugin remove ionic-plugins-keyboard
# 这样子ok,remove plugins下面的目录名字,也就是他的plgin名字
## 没有使用 sudo cordova plugin remove ionic-plugin-keyboard

## by wzh 20160308 联系人
sudo cordova plugin add org.apache.cordova.contacts

## ios 发布
## 没有使用 sudo cordova plugin add https://github.com/phonegap/ios-deploy

## 20160408 因为苹果审核问题,升级keyboard
sudo ionic plugin rm ionic-plugin-keyboard
sudo ionic plugin add ionic-plugin-keyboard

## 20160708 android 版本升级
sudo cordova plugin add https://github.com/whiteoctober/cordova-plugin-app-version.git

## window.navigator.dialogsPlus
sudo cordova plugin add https://github.com/laden666666/cordovaDialogsPlus



