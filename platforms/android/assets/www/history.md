## ToDoList 
3、注册
4、数据库
5、手势
微信
缓存处理
http://www.haomou.net/2015/08/12/2015_loopback_rest/

## 20160728 首页 上拉下拉刷新问题
    1 怎么会永远不停呢？
    2 应该有个 destroy 事件
    3 不要发错广播
    4 没有数据了,那个进度条怎么让它消失?死在那里了!
    5 在手机上怎么会同事触发上拉\下啦刷新呢 ?
    6 如果加载的数据没有充满屏幕，会自动再调用一次,所以每次多要点数据,本次改成15
    7 去掉上面的sub header和地下的 tabs,他们挡住了上拉\下拉的进度条
    8 造成冲突的原因:refreshComplete 和 infiniteScrollComplete 广播是异步完成的
    9 应该增加 on pull 事件,提示:没有数据了!
    10 怪事!手机还有串门?
      ion-infinite-scroll 的 ng-if 加上 ng -show试试
      串门少了
    11.首次种子加载之后,不显示 ion-infinite-scroll
    12.还真的好复杂!
      应该想办法解决 2 个刷新之间的广播异步问题,而且是第一次feed也有影响
    13. feed 改大一些:25 distance改大一些 30%  
    14. 可能是feed 数据没有显示完造成的吗？那是不是应该 feed 减小?错了!越小串门越多
    15. 改成 feed =40 了,切换到别的页面之后再回来,就不串门了
    16. 回复menu.html的sub header和地下的 tabs,以后再继续
      
## 20160727 app 升级优化
    1. ionic 的 loading 总是不舒服,换成一个dialog
      window.navigator.dialogsPlus
      参考:[http://www.cnblogs.com/laden666666/p/5510625.html]
      下载插件: sudo cordova plugin add https://github.com/laden666666/cordovaDialogsPlus
    2. 升级 ionic.js LIB
    3. 修改 init.sh
    4. 发现一个中文网站,中文ng cordova 比较详细
      http://www.ionic-china.com/index.html
      
## 20160726 杂项学习
    1. 更新已建ionic项目中的js类库，
       命令行中先进入项目所在目录，然后运行
       sudo ionic lib update 
    2.展现ionic项目结果
      ionic serve --lab 
    3 转移 init.sh 到根目录,并设置可运行  
      chmod +x init.sh 
      ./init.sh

## 20160724 忽略 platforms 文件夹
    1 以后不再上传 platforms 目录
    2 下载后,自行增加平台\编译
      增加3种平台方法
        sudo ionic platforms add android
        sudo ionic platforms add ios
        sudo ionic platforms add browser
      编译
        sudo ionic prepare android
        cordova compile android 
        cordova compile android --release
        sudo ionic compile browser
        sudo ionic compile ios
      然后运行
        android:
          sudo ionic run --device android
      苹果(需要安装 ios-deploy)
        sudo ionic run --device ios

## 20160723 苹果升级问题
    1 判断是否是 ios
      参照 http://caibaojian.com/browser-ios-or-android.html
      var u = navigator.userAgent;
      var isiOS = !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/); //ios终端
    2 苹果只能跳转到 下载地址(store),利用浏览器更新
    3 我们的api 没有返回 ios 的 下载地址,不同的 app自行手工修改
      var url = "m.dhbm.cn/app/qy/lmsd/ios/"; //可以从服务端获取更新APP的路径
      window.open(url, '_system', 'location=yes');  

## 20160723 升级 app 改进
    1 读取apk版本号放在 cordova 读取app版本号之前
    2 这样在 pc 端一样可以调试

## 20160720 升级 app
    1 popup 里面放进去一个btn,scope 走不通
    2 popup 厘米啊你放进去一个 href,可以跳过去
    3 route 做一个只有 ctrl,没有template会怎样?
    4 以上问题都是因为 popup 造成
    5 准备使用 $ionicModal 来完成 about和uograde
    6 目前问题:about里面不能响应btn,下载过程不能取消
    7 当前版本号的css设置不好,待处理
    8 先去完成 cdv 之后,再来对付这些!
    
## 20160719 升级 app
    1 升级app 放在 services 里面
    2 version="1.0.160708" ,如果版本号变小了,run会提示错误!
    3 popup 样式总是整不好!

    
## 20160718 计划:拆分聊天部分,重新创建一个 ionicChat App

## 20160718 app 升级处理
    
## 20160718 放弃 cordova插件 
    1 修改插件源码应该是 src目录下的 android 目录下 的 java
    2 插件根目录下的android目录是生成的,改了没用
    3 自定义 cordova 插件以失败告终
    4 实际写插件的话,还是需要先建立一个原生的app,测试过这些插件功能之后,才可以续写plugin
    5 plugin 里面不要使用全局变量 
    6 失败了,删除掉 cordova-plugin-app-update 目录
    
## 20160712 cordova插件使用 , ng-Cordova 疑问
    1 刚刚发现,ng-Cordova.js 里面已经加入了 $cordovaAppVersion 插件,并且做成了 factpry
    2 是不是引入了 ng-cordova之后,那些 cordova plugin 无需再手工加入?待实验
    
## 20160712 app update 插件安装(android)
    1 简单起见,copy 整个 cordova-plugin-app-update 目录到当前工程目录
      如果外面修改过,需要重新 copy,卸载\安装
      不 copy也可以,记住插件目录位置,好处是:外面修改后,执行卸载\安装
    2 安装插件 
      sudo cordova plugin add cordova-plugin-app-update
      或者 sudo ionic plugin add cordova-plugin-app-update
      卸载命令
      sudo cordova plugin remove cordova-plugin-app-update
      或者 sudo ionic plugin remove cordova-plugin-app-update
      
## 20160712 app update 插件制作(android)
    1 复制一份 cordova-plugin-app-version,作为原型修改
    2 修改 包名 com.dhbm.appUpdate.cordova
    3 修改 class 名字 AppUpdate
    4 增加一个 action,病复制原来的 update部分 代码
      if (action.equals("checkUpdate")) {
              this._checkUpdate(args.getString(0), args.getString(1), callbackContext);
                return true;
            }
    5 修改js借口文件名字为 AppUpdatePlugin.js    
    6 修改 config.xml定义,模块名\节点名改成
        <js-module src="www/AppUpdatePlugin.js">
           <clobbers target="cordova.appUpdate" />
        </js-module>
    6 修改 config.xml,id名称改为 
      id="cordova-plugin-app-update"   
    7 修改 AppUpdatePlugin.js ,exec参数改成我自己的plugin名字 AppUpdate
      cordova.exec(success, fail, "AppUpdate", command, []);
    8 保留了以前的appVersion的所有方法,增加新方法
      getAppVersion.checkUpdate = function (success, fail) {
        return getPromisedCordovaExec('checkUpdate', success, fail);
      };
    9 修改 export 模块名字 ,对应 config.xml 的 target 名字
     module.exports = appUpdate;
    10 磁盘读写权限设置怎么办? 
    
## 20160709 android 版本升级处理-跨域设置
    1 设置 iis 6 服务端,api get 允许跨域
      如果考虑安全性,正式发布可以再去掉
    2 参考 http://virusswb.blog.51cto.com/115214/1128395
    3 增加以下3个 http header,
      Access-Control-Allow-Origin *  
      Access-Control-Allow-Headers *    
      Access-Control-Allow-Methods GET, POST, PUT, DELETE"   
    4 如果是 iis 7 ,直接修改 web config,在 system.webServer 节点 增加以下节点即可
      <httpProtocol>    
        <customHeaders>    
          <add name="Access-Control-Allow-Origin" value="*" />    
          <add name="Access-Control-Allow-Headers" value="*" />    
          <add name="Access-Control-Allow-Methods" value="GET, POST, PUT, DELETE" />    
        </customHeaders>    
      </httpProtocol>  
      
## 20160708 android 版本升级
    1 增加 cordova 插件
      cordova plugin add https://github.com/whiteoctober/cordova-plugin-app-version.git
    2 在 Ctrl (我这里放在SettingCtrl) 引入 $cordovaAppVersion 
    3 新建并且在app.js 注入 'starter.services.getApkVersion'
    4 index.html 加入 getApkVersion.service.js   

    
    
## 20160628 android 打包问题
    1 新建项目(或者删除之前的android目录),增加 android平台
      sudo ionic platforms add android
      sudo ionic prepare android
    2 修改权限,必须保证 android 目录可读写  
    2 在 AS 打开 android
    3 下载 
          //add by wzh 20160627 Cannot resolve symbol AsyncHttpClient
              // http://stackoverflow.com/questions/30956624/cannot-resolve-symbol-asynchttpclient
              // 增加 android-async-http 依赖
              // http://loopj.com/android-async-http/doc/com/loopj/android/http/AsyncHttpClient.html
              // http://loopj.com/android-async-http/
              compile 'com.loopj.android:android-async-http:1.4.9'
    4 copy 该 jar 到 Libs 目录
    5 修改 build.gradle 文件,增加 依赖
          compile 'com.loopj.android:android-async-http:1.4.9'    
    6 AsyncHttpClient 错误 ,ALT + Enter ,接受 import
    7 Header 错误,接受第二项 :  import cz.msebera.android.httpclient.Header;
    8 确保 样本 MainActivity.java,AS 中没有错误  
    9 以上完成,ok!
      sudo ionic compile android
      sudo ionic run --device android
    
## 20160617 继续 网络状况不好的处理
    1 错误提示 connection to the server was unsuccessful 
    2 android 的 mainactivity.java 的 loadUrl加上10秒超时
      loadUrl(launchUrl,10000);
      setIntegerProperty("loadUrlTimeoutValue",10000);
    3 ios待处理,网上建议 加一个小 index.html,待处理
       <script>
      window.location="index.html";
      </script>
    4 去掉index.html 中 不必要的 加载
    5 不灵啊!编译就报错!
    6 直接在 项目目录(不是platform)config.xml加上该参数
      <preference name="LoadUrlTimeoutValue" value="10000"/>
    7 人家缺省是 20秒,10秒能解决什么?
    8 待观察处理吧
    9 观察结果更糟!列表内容不出来了,那里设置的20秒,现在外面设置 10秒?
    10 还是还原回去吧!应该是因为 index.html 夹在了找不到的插件造成错误
    
    
## 20160616 网络状况不好的处理
    1 NewPlayers 的 promise 都加上error 处理,直接返回 null
    2 PlaylistsCtrl 的 NewPlayers.getFeedPlayers 也同样增加 null 处理
    3 飞行测试,发现 tab切换不太管用了!难道非得有网?待处理吧
    4 取消飞行,点重来 也不动?
    5 再次实验,没有该问题,可能是 幻灯 部分造成的
    8 给 SlidersCtrl加上返回 null时,这里也是空,先简单返回
      果然如此!
    9 飞行,再取消飞行,回到首页,应该出来新数据!这个待处理
    
## 20160616 遇到升级问题
    1 错误提示
      ✗ (node:727) fs: re-evaluating native module sources is not supported. If you are using the graceful-fs module, please update it to a more recent version.
    2 查看 graceful-fs
      npm info graceful-fs -v
      npm ,graceful-fs 都是 3.6.0
    3 升级 npm ,会同时升级 graceful-fs
      单独升级 graceful-fssudo npm update  graceful-fs  -g
      npm ,graceful-fs 都是 3.9.6
    4 从外面删除 android目录,重新增加 android platform
      ionic platforms add android
    5 除了以上错误,还出现心错误
      Error: EACCES: permission denied, open '/Users/dhbm/.config/configstore/insight-cordova.json'
      You don't have access to this file.
    6 找到该文件,修改权限 
      sudo chmod 777 insight-cordova.json
    7 再来重新增加 android platform
      还是出现 graceful-fs 错误,先忽略,继续
    8 准备编译 sudo ionic prepare android
      也还是出现 graceful-fs 错误,先忽略,继续
    9 运行 sudo ionic run --device android
      一样出现 graceful-fs 错误,先忽略,继续!哈哈哈,反正可以运行了,先继续

## 20160603
    1 升级 ionic 到 1.7.15
    2 学会一个 ionic 命令
      ionic resources 
      或者分步执行
      ionic resources --icon       
      ionic resources --splash 
      
## 20160527 这个demo 就到这里吧,聊天部分单独做一个app

## 20160425 又闲置了好几天,继续
    1 服务端没有开启时,io错误应该弹出提示,省得不知道发生了什么
    2 服务器 ip 将 127 改为 192,以便手机测试
    
## 20160420 继续聊天
    1 messageDetai都改名叫 chatDetail,聊天所有相关的名字都以chat开头
    2 关于登录和 socket,是不是应该放在 chatRoot 里面完成?待处理
    3 socket 相关变量都放在 rootScope,大家都可以使用?
    4 chat 相关对象统一定义为一个公共对象?
    5 整理一个 ionic-wechat-master 数据结构(LS保存的)
    6 ChatRoom-angularJS-master 没有保存数据,数据结构什么样?
    7 干脆相爱早 ChatRoom-angularJS-master,加上 LS
    
## 20160419 继续聊天
    1 socket 连接放在 messageDetail 里面
    2 聊天记录实际都是读取的LS 内容,无需 socket
    3 ChatRoom-angularJS-master 的大部分内容其实应该在 messageDetail 里面
    4 先假设都是 群聊,发送,接收可以看到 log 了
    5 console.log 如果现实 object,直接单独一行,今天才学习到,惭愧!
      console.log(data);
    6 下一步: 发送\接收之后 refresh,clear,写入LS
    
## 20160419 继续聊天
    1 messageDetail 是二级页面
    2 页面布局问题,ion-view,ion-header-bar,ion-content 等什么关系?
    3 因为聊天现在不是独立的app,而是嵌入在app里面的另一个风格的界面,单独给他设计一个外壳 chatRoot.html
    4 建立 chatRoot.ctrl.js,作为聊天的根 ctrl
    5 roote 增加一个chatRoot的 abstract 路由
    
## 20160418 伪造聊天数据
    1 app 首次进来,判断本地 LS,如果没有 userName,直接从本地读取并写入,下次就有了
    2 同理,伪造聊天数据,并进行预处理,为了之后进行过滤\查找\删除
    3 抄写他的 directive,完成界面呈现
    4 学习  Sass,先抄写人家的 ionic.app.scss
      http://www.ruanyifeng.com/blog/2012/06/sass.html
    5 学习 wechat 范例
      http://www.tuicool.com/articles/RFzmy2q
      
## 20160418 继续聊天
    1 抄写 ionic-wechat-master的界面\数据和img
    2 改造数据,增加一个userName字段作为登录用户名,原 name 作为昵称 
    3 改造login,登录用户名必须存在于 friends
    4 改造 friends 数据,从 api copy 10条数据,并修改 userName
    
## 20160414 聊天界面在手机上不合适
    1 index.html 的socket.io 的服务器地址改成绝对地址 192.168.1.199
    2 使用app本身的登录数据 
      var loginData = Globals.mLoginData;
    3 从 ionic-wechat-master 抄写界面吧,那个适用于手机
      进来的时候显示 tab-message.html
      点进去的时候显示 message-detail.html
      什么地方显示联系人 ? tab-friends
    4 已经存在的聊天信息保存到 LS
    5 手机进来,空白!没有信息
    6 只要有过聊天信息,  tab-message就会有内容了,这个也要保存在 LS,还是查询出来的呢？
    7 是不是要使用 webDB了？W3C目前力推的H5本地数据库是IndexedDB
      参考 http://www.6ddd.com/wz/zxwz/13012.html
      http://www.myexception.cn/nosql/1973758.html
      http://www.ithao123.cn/content-10542920.html
    8 下一步使用 cordovaSQLiteSource  
        
## 20160413 闲了好多天了,还是把这个nodejs 聊天搞定吧
    1 运行,找不到 io
    2 package中加入 socket.io,并重新 npm install
    3 运行 服务端,客户端需要引入的是服务端的那个socket.io,本地调试如下
      <script src="http://127.0.0.1:3002/socket.io/socket.io.js"></script>
    4 copy 人家的app.css,并引入
    5 改造 chat.html
    6 布局不对,待改造 directive
    
## 20160408 准备加上 nodejs 聊天
    1 服务端直接使用人家的ChatRoom-angularJS-master
    2 客户端部分转移到我的app
    4 分解他的 directives和serices,copy过来,创建以下js,并引入app
      socket.service.js
      randomColor.service.js
      userService.service.js  
      message.directive.js
      user.directive.js
      message.html
      user.html
      chat.ctrl.js
    5 在 setting 增加一个chat聊天,对应布局怎么办?
    6 需要增加的功能:
      1) 登录采用已有的
      2) 如果已经登陆,自动连接socket,端口:3002
      3) 先使用手动连接:点聊天的时候,去判断登录,去连接socket
      4) 布局照抄先
    7 在 index.html 引入以上添加的 js文件
    8 在 app.js 引入以上添加的所有模块
        
## 20160408 升级keyboard插件
    sudo ionic plugin rm ionic-plugin-keyboard
    sudo ionic plugin add ionic-plugin-keyboard
    之前是1.0.8版本.升级到2.0.1
    
## 20160318 告一段落,其他功能以后再说

## 20160318 联系人demo过于简单,以后用到再说 
                 
## 20160317 其他问题
    1 $scope.$on('$destroy' 不反应,因为ionic升级后,换成其他事件了
      ***参考 http://ionicframework.com/docs/nightly/api/directive/ionView/
              http://ionicframework.com/docs/api/service/$ionicPlatform/
                  
## 20160317 回退键问题处理
    1 先处理到这里了:登录使用了popup方式,会出现 两次回退退出程序,不太合理,不知如何判断,待处理
    2 软键盘关闭之后1秒才会改变visible参数,也许非常快的连按2次会有问题,忽略!
    3 目前这样比较合理,除了popup之外
    
## 20160315 回退键问题处理
    1 阅读 plugins目录下 ionic-plugin-keyboard/www/android 目录下 keyboard.js
    *** 以前注意力在 ionic-plugin-keyboard/java ,以后应该先找js
    2 找到 'native.keyboardshow' 'native.showkeyboard'事件,哈哈哈!可以监控了
    3 通过监控来控制 回退事件,ok!软键盘在,不要回退,只close
    4 var isVisible = $cordovaKeyboard.isVisible()是错误的,他只是一个属性值
      *** 应该这么样 var isVisible = $cordovaKeyboard.isVisible
    5 popup actionsheet  等优先级怎么办?怎么判断当前是什么type 的 view ?待处理
            .constant('IONIC_BACK_PRIORITY', {
              view: 100,
              sideMenu: 150,
              modal: 200,
              actionSheet: 300,
              popup: 400,
              loading: 500
            })
    6 子页面自己单独定义一个listen,会怎样?谁会优先?还是都会处理?待处理  
    
## 20160315 联系人
    1 cordova plugin add org.apache.cordova.contacts
    2 测试发现一个键盘问题:输入时,软键盘出现,原来的回退键盘成向下,好像还是回退了?
    3 如果还是回退了,那么应该在输入时,屏蔽回退功能
    4 先试试cordova键盘事件
    5 还是要加上 cordova keyboard插件啊?
    
## 20160314 再定义一个root
    1 只保留 header 和 sub-header
    2 回退需要在 single 的 ctrl 完成
     
## 20160314 给 playlist 换个 abstract 壳
    1 重写一个 menuWithoutTabs.html,作为playlist的壳
    2 routes.js 增加一个 single 抽象路由
    3 playlist路由转到 single子页面
    4 再试试不要任何壳的页面,也就是他的root不是abstract
    ** 去掉 抽象页面的 single 
    ** 得到错误 Could not resolve 'single.show' from state 'app.playlists'
    5 结论:不能直接传递参数,如果换成其他方式传递参数过来,也许可以
    6 single 也是根,怎么给他加上一个 回退?
    7 特意在 menuSetting.html中,增加了2个以single为根的功能:检测设备,图片选取
    
## 20160312 inspect code (只对js目录)
    1 缺少 ; 都是该补上的
    2 unused unresolved 视情况而定
    
## 20160311 不断加载怎么处理?
    
## 20160311 重新组织项目结构
    1 ctrl directive service
    
## 20160311 修改css
    1 空出底部 tabs 地方
      <ion-nav-view style=" bottom:50px " name="menuContent"></ion-nav-view>
    2 顶上空出,需要在显示小标题的时候动态处理 ,待下次处理
    
## 20160311 重装 node 5.6.0
    1 使用 nodejs官网下载的pkg,安装后,ok
    2 npm list 最后提示:missing: gulp-util@^3.0.0, required by gulp@3.9.1
    3 sudo npm install gulp 之后 ,npm list ,ok!
    4 等下次使用 gulp 再来继续学习!
    
## 20160310 npm坏了
    1 昨天执行了npm升级,完蛋了!sudo npm install -g --upgrade npm
    2 npm install 自己就出错了! Darwin 14.5.0
      *** 不行! brew reinstall node --with-full-icu
      *** 不行! 在下载目录 node-v5.0.0.pkg 直接运行
      *** 重新下载 4.3.1版本,可以运行了!但是缺少很多(npm 2.14.12)
      *** 不行! 使用 4.3.1 sudo npm install -g ios-deploy
      *** 以下操作全都是白
        sudo npm install -g ios-deploy
        npm install -g ios-deploy --unsafe-perm=true
        sudo npm install -g --upgrade npm --unsafe-perm=true
        npm install --unsafe-perm=true
        
        升级node
        npm install -g n
        n stable
    3 下载最新版 5.6试试
      
## 20160308 加上连续2次回退,退出app
    1 参考 http://ionichina.com/topic/5514b539b6421f9166aa5f88
          http://www.bubuko.com/infodetail-709440.html
          http://www.mamicode.com/info-detail-1151536.html
    2 该exitApp实际调用了浏览器的退出,navigator.app.exitApp!
    3 所以,下次再来splash不会出现,因为程序没有干净退出
          
## 20160308 突然发现 splash 怎么不出来了？黑屏
    1 经过检验,缺省的 splash time = 3秒就是不行!5秒,10秒是可以的
     <preference name="SplashScreenDelay" value="5000"/>
    2 只有强行停止app之后,再次运行,才能看到splash
    3 加上 连续2次回退,退出,看看是不是干净的退出app了?干净了才会下次出来splash
    4 splsh时间内必须 deviceready!否则就会看到 deviceready has not fired after 5 seconds
      ***所以,建议设置的大一些!我改成 8秒
      
## 20160308 联机有问题
    1 sudo ionic run --device android 和 adb monitor 不一致
    2 webstorm 设置的 ANDROID_HOME=/Users/dhbm/android-sdks
      Version: 24.3.4 
    3 我是用的是另一个目录下的 Version: 23.0.2 
    4 务必保持一致

## 20160308 去掉键盘扩展插件
    1 发现 app.js 中已经存在 cordova 的 keyboard处理!
    2 查找后发现,在 www/lib/ionic/js中确实已经存在 
    3 git 方式下载的插件怎么 remove?
    4 remove 这个 ionic-plugins-keyboard,会不会也会影响 ionic?应该不会吧,试试!
    
## 20160307 继续 微信
    1 参考 http://bewithme.iteye.com/blog/2258199
    2 index.html 中引入 jweixin-1.1.0.js
    3 在 app.js 初始化时,配置 wx api
    4 测试号信息 http://mp.weixin.qq.com/debug/cgi-bin/sandboxinfo?action=showinfo&t=sandbox/index
      微信号： gh_252f398d195a  
      appID   wx84c58aa8c921ba93
      appsecret 930bcac129718f6a960c3f346eb38d65
    5 自己先关注,下次再天假别人
    
## 20160307 键盘扩展插件
    1 下载插件,命令加入 init.sh ：
      https://github.com/driftyco/ionic-plugins-keyboard.git
    2 为什么要用?
      开发过hybrid APP都知道，使用表单时键盘的弹起/关闭中会引来很多问题，
      Ionic之前的方案都是js实现，效果实难恭维，上述插件以原生代码的方式来优化这个问题，
    3 怎么测试?我现在只有一个login有input,它本身是一个$ionicPopup,看不出效果 下次做了其他input再试试
 
## 20160307 cordova 原生提示插件
    1 下载插件,命令加入 init.sh 
      cordova plugin add https://github.com/EddyVerbruggen/Toast-PhoneGap-Plugin.git
    2 间翻处理,仅作提示(忽略提示功能出错),放到 services.js
    3 在 menuSetting.html 放一个 测试 button
    4 新做一个 toastTest.html 页面
    5 $cordovaToast.showLongCenter(message)不带后续参数也可以使用啊!! 待删除factory
    
## 20160306 以后都在 history.md 文件写这些玩意儿了
    README 将只保留 安装和使用的 log
    
## 20160306 随意测试
    1 下方tabs 样式不对了!图形下方文字没了,设备联机运行,5个tab item才显示 4 个
      ** 经查,是上次做 WX 分享,在 index.html的body增加了一个 <img>,造成错误
    2 WX功能 准备使用js sdk,采用测试者账号
    3 ng-href=#/app/localNotification>推送通知 , 没有引号,也对啊?按规范,加上!
    4 底下的 tabs 是不是该 Footer?icon 是不是可以 png?待处理
     
## 20160306 闲来无事,测试一下h5 Api 支持情况
    1 参考 http://www.tuicool.com/articles/vInEv2,复制代码到 h5NetworkApi.html
    2 测试我电脑上 safari,Chrome ,都不支持
    3 测试我手机上 QQ浏览器,UC,居然也都不支持
    4 测试微信,天啦!他可以支持,但是返回的网络类型:undefined,说明支持不完整
    5 试试 cordova 会怎样?在menuSetting.html加一个<a>
      *** 不是ng写的html.就没有# ! ng-href='templates/h5NetworkApi.html'
      *** 很遗憾!也不支持!
    
## 20160304 Web发布
    1 查询支持的平台是否包括浏览器
      sudo cordova platform list
    2 增加 browser 平台
      sudo ionic platform add browser
      或 sudo cordova platform add browser
    3 预编译,没有发现什么变化
         sudo ionic prepare browser
    4 编译,
       sudo ionic compile browser
      结果生成了一个 platforms/browser/build/package.zip
      但是,解压错误!"无此文件或目录",以后再处理
      
## 20160302 WX分享
    1 找了一个 weixinApin 4.3版,放进 lib/weixin 目录
    2 index.html 引入 weixin43.js
    3 index.html 引入 weixin.ctrl.js
    4 base64 插件不用了,从app.js和SettingCtrl去掉,安装的base64插件下次在整理的时候去掉
    5 可能 ng 的 wx 分享不是这么做的?
    6 新找的 weixinApi 4.3不行啊!换回2.5
    
## 20160302 推送 通知
    1 后半截任务是这么完成了,即:通知到了!前半截怎么办?即:谁来推送?
    2 准备使用百度推送,怎么接口?待处理
    
## 20160302 推送 通知 
    1 先做一个简单的 本地通知 localNotification
    *** 直接从ng-cordova demo 中 copy 代码
    2 添加插件,同时将命令加入 init.sh
      cordova plugin add https://github.com/katzer/cordova-plugin-local-notifications.git
    3 在 app.js 引入 'starter.localNotification.ctrl'
    4 在 menuSetting.html加一个<A> ,在 routes 加上 路由
    5 在index.html 加上,因为这次是独立的文件
      <script src="js/localNotification.ctrl.js"></script>
      
## 20130301 幻灯一次打开的条数太多,下次缩减到最多10个
    简单截取10个
    $scope.playlists = Globals.mPlayLists.slice(0,10);
## 20160301 回到Webstorm 联机测试
    1 测试联想手机是,偶然发现没有网络就运行之后,总黑屏\发生怪事等等
      *** 怎么办?对于需要网络的功能,必须预防!否则,容易造成数据混乱
      *** 待继续测试,本次是重启手机之后恢复正常的
    2 adb devices 可以检测是否脸上手机
      *** 如果没有看到手机,就不要测试了 
      *** 联想手机logcat飞似的log,不建议使用他调试,华为不错  
      *** 运行命令: ionic run --device android
    3 adb logcat 可以输出 console.log 等信息,但是一次只能有一个程序监控到
      *** android device monitor,Eclipse,Android studio 都会和adb logcat冲突
      *** 一旦冲突,容易谁都出不来,只能拔掉usb再来
    4 要么 web storm + android device monitor,要么使用 studio 打开,一次只用一个
      *** 同时开启,后开启的 android studio 会提示: adb error
    5 只有在需要开发 cordova 插件的时候,采用 studio 比较合适(启动很慢)
    
## 20160301 android Studio 直接打开
    Open Existing Android Studio Project
    连接手机,确认开启 开发者调试
    在 Terminal 下执行
    ionic run --device android
    您就可以在手机上看到结果啦
    
## 20160301 导入到 android Studio
    1 只导入 android 部分
      在Studio 选择 import project(Eclipse ADT,gradle etc) ,选中以下 gradle文件即可
      platforms/android/build.gradle
    2 在菜单 Run即可
    
## 20160229 imagePicker 插件问题
    1 上粗 init.sh 里面的 imagePicker 插件没有安装成功,重新来过
      cordova plugin add https://github.com/wymsee/cordova-imagePicker.git
    2 没有安装 imagePicker,执行 选取图片,容易造成死机,是不是程序应该预先判断?或者try catch?待处理
    3 最好保证网络畅通,插件都成功
    
## 20160229 处理 splash
    1 经过来回折腾,最后重新建立了工程 ion20160301
    2 经过试验,还是将 splash 隐藏工作自动完成,即:
      <preference name="AutoHideSplashScreen" value="true" />
    3 高级功能 Fadein 等,暂时不要设置了,不知道是手机问题还是cordova 问题,以后再处理
      ** 这2 项先不用
      <preference name="FadeSplashScreen" value="true"/>
      <preference name="FadeSplashScreenDuration" value="15"/>
  
## 20160229 整理 plugins 到init.sh
    sudo sh init.sh
    不对啊,全进来了!
    
## 20160229 处理 splashscreen
    1 强制在app.js 里面 navigator.splashscreen.show()
      ** 先黑屏2秒左右,才出来 splash,且不会消失
    2 接受建议 http://cnionic.com/web/index.php?s=/issue/index/issuecontentdetail/id/31.html  
    3 更糟糕!直接崩溃！
    4 删除 android,重新生成一次试试!因为 android 的 preference中,没有假如刚才那几个 splash 参数
    5 越变越惨!死了!
    6 到外面 terminal 重新来过,这次删除 platform 目录
    7 惨!去掉 splash 再试试!也一样!
    8 重新安装所有插件 plugins
    
    
## 20160229 试试 分享
    1 增加 SocialSharing 插件
      不要啦! https://github.com/EddyVerbruggen/SocialSharing-PhoneGap-Plugin.git
      *** 阅读源码,找找 WX,Weibo 能不能嵌入
      *** 找不到地址了,下次再试试
      
## 20160228 加上 splashscreen
    1 增加 splash 插件
      cordova plugin add cordova-plugin-splashscreen
    2 黑屏,待处理
    安卓要修改 platform/android/cordova/default.xml ios要修改 platform/ios/cordova/default.xml
## 20160228 日期选择
    1 增加 datepicker 插件
      cordova plugin add https://github.com/VitaliiBlagodir/cordova-plugin-datepicker.git
    
## 20160228 画廊
    1 先做一个图片选择列表
    2 增加 imagePicker 插件
      cordova plugin add https://github.com/wymsee/cordova-imagePicker.git
      上次 Camera 的时候,已经增加该插件了(如果没有,请增加)
    3 记得引入 $cordovaImagePicker
    4 做假数据遇到问题:Duplicates in a repeater are not allowed 因为相同的内容重复引起
      *** 按照提示,加上 track by $index,又触发安全错误!先这样子吧,以后再处理
      *** 做测试用的假数据也要小心啊
      
    
## 20160226 网络监控问题
    1 休眠不断网怎么设置?
    2 先处理断开后刷新
      测试方法:飞行,再取消飞行!
      cordova 只在网络状况发生变化后才能反应,直接断开的第一次他检测不到?
      *** 发现是我自己写反了!
    3 手动恢复方式 ok!加上自动恢复
      *** cordova 监控会自动恢复 
      *** 但是,那个 DOM 怎么办? hide 和 remove 什么区别？待处理
      
## 20160226 上传图片继续
    1 更换上传 api 
    var url = "http://fangche.dhbm.cn/index.php/Api/Upload/pic_upload/?resulttype=json";
    2 因为使用了 FILE_URI,所以上传完成，显示之后,需要 cleanup,否则不能连续操作
      $cordovaCamera.cleanup().then(...); // only for FILE_URI
    3 从相册中取出来,再剪切之后,文件名加上了一个 ??1456482798503 之类后最,需要预处理掉
      file:///storage/emulated/0/Android/data/com.dhbm.demo123/cache/IMG_20160226_165859.jpg?1456482798503"
    4 upload之前对文件名进行过滤,ok!
    5 点击 button,首先清除掉 "上传成功提示"
    6 连续拍照或相册并上传,有时会失败!再来一次
    
## 20160224 上传图片继续
    1 ng-cordova 的 file up;oad 使用的是post,上传的事文件,而不是 base 64
    2 plugin 程序 的log TAG = FileTransfer,在 Android Device Monitor 中过滤 FileTransfer 查看
    3 我们的 api 只接收 base64,只好使用 $http.post 试试
    
    *** api 太简陋
    4 这是以前使用过的 api
    public static final String ApiKey = "3414d916c27e14bc3e9e08a30660d5ec268e76d009aee2a2c5a828973f40d223";
    Url_UploadImgSyq111 = "http://sh.dhbm.cn/api/pic/upload/?key="; // 手机上传图片接口(生意圈使用)
    String UploadUrl=DDUrl.Url_UploadImgSyq
     				+AppSet.ApiKey    	 				
     				+"&filename="//如果已经是bmp，随便取个文件名字吧
     				+"&imgwidth="+GG.UrlEncode(""+imgwidth, "gb2312")
     				+"&imgheight="+GG.UrlEncode(""+imgheight, "gb2312")
     				//+"&imei="+GG.UrlEncode(imei, "gb2312")
     				+"&appid="+GG.UrlEncode(AppSet.App_Id, "gb2312")
     				+"&i="+imei
     				+"&mobile="+GG.UrlEncode(mobile, "gb2312")
     				+"&upflag="+GG.UrlEncode(upflag, "gb2312")
     				+"&encoding=utf-8";
    5 现在怎么办?
     				
## 20160222 上传图片继续
    1 浏览器端存在跨域 ,以后再处理,使用 open 一个新的 window,或者使用 iframe
      互相共享数据,close回来原地,下次在处理
    2 在手机app打不开 window.open('http://192.168.1.100:8000/index.html');  
    3 location.href 再试试
    4 改用 $cordovaInAppBrowser
     cordova plugin add cordova-plugin-inappbrowser
    5 打开啦!但是
    6 file chooser 空!选不了文件!在手机怎么选相册?应该是使用 img 的 uti,而不是 file 的uri
    7 Api 应该是 http://yltt.dhbm.cn/syssysmanage/index.php/Api/Upload/?resulttype=json&callback=result&imgData=data(图片内容)/ 
    8 Api 问题 还是我的 app 问题?待解决后整理代码
    
## 20160222 上传图片
    1 重新下载了一个 angular-base64-upload 模块
      https://github.com/adonespitogo/angular-base64-upload
    2 安装方式
       - Bower - bower install angular-base64-upload
       - NPM - npm install angular-base64-upload
    3 本次使用了 npm 安装,angular-base64-upload 模块出现在 node-modules了
    
## 20160222 上传图片
    1 找了一个现成的服务端 api
      http://yltt.dhbm.cn/syssysmanage/index.php/Api/Upload/pic_upload_base64/?resulttype=json
      
    2 先做一个pc上的图片上传,简单起见只上传一个本地文件 img/ionic.png
    3 如果实际在pc使用,需要一个input 选择文件,这里忽略,仅仅为了手机相片上传做测试
    4 安装一个 base64 插件
      bower install angular-base64
      *** 在 www/lib/下多出来 5个目录,太多了!我只要一个来着,不用的是不是可以删掉?待处理
      /angular
      /angular-animate
      /angular-base64
      /angular-sanitize
      /angular-ui-touter
    5 给 'starter.controllers' 模块注入 ['base64'],因为功能在这个 module 里面
      angular.module('starter.controllers', ['base64'])
    6 给 SettingCtrl 注入 $base64,因为功能在这个 ctrl 里面
    7 $base64 包含以下2个功能
      $scope.encoded = $base64.encode('a string');
      $scope.decoded = $base64.decode('YSBzdHJpbmc=');
    8 <script src="lib/angular-base64/angular-base64.js"></script>
    9 这个angular-base64 不是供图片上传用的
    10 他那个base 64 模块是一个外部的 module,如何引用并使用的?下次在学习
        
## 20160221 给 主页加一个副标题,网络断开才显示
    1 menu.html 中 ion-nav-bar 之后加一个 ion-header-bar
    2 这 2 个什么区别？
    3 麻烦啊!隐藏/显示切换后,他占的位置消失了？下方的内容位置错了
    4 监控是在app.js,在rootscope,他怎么让app知道?rootscope能不能直接绑定?
    
## 20160221 upload 需要一个服务端api
    1 不能总发布到手机app才能调试,先做一个网页版客户端
    2 nodejs 做一个服务端
    3 待学习!
    
## 20160221 css 问题
    1 在android手机上,进度条,actionSheet等都没有居中
    2 网上建议,我试了不行
     这是网上建议: 改ionic.css  把2977-2999行注释掉 
    3 lib目录下 scss里面的那些css起什么作用?应该可以该这里?亦或ionic 的css可以自定义
    4 和Bootstrap一样，ionic也提供了栅格系统。不过ionic的实现是基于CSS3的FlexBox 模型，更为灵活
    
## 20160220 继续图片上传
    1 从相册选取,增加插件 
      cordova plugin add https://github.com/wymsee/cordova-imagePicker.git
    2 option 组合测试结果,总共7种组合,这里列出4种
      1) destinationType: Camera.DestinationType.FILE_URI
      allowEdit: false,//
      saveToPhotoAlbum: true,//false,
      照相 ok 相册 ok
      
      2)destinationType: Camera.DestinationType.FILE_URI
      allowEdit: false,//
      saveToPhotoAlbum: true,//false,
      照相 ok 相册 fail
      
      3)destinationType: Camera.DestinationType.DATA_URL,//
      allowEdit: false,//
      saveToPhotoAlbum: true,//false,
      照相 显示不出来 相册 显示不出来 ,当然也无法获取大小
      
      4)destinationType: Camera.DestinationType.DATA_URL,//
      allowEdit: false,//
      saveToPhotoAlbum: false,
      照相 显示不出来 相册 显示不出来 ,当然也无法获取大小

    3 总结:
      1 如果从相册选取 allowEdit 只能是 false
      2 从相册选取,连续操作,还需要 cleanup,照相则不用
      3 照相可以压缩,可以裁剪,相册只能压缩
 
## 20160220 获取图片大小部分代码复用
    1 转移到 services 部分代码先保留一段时间,也没有使用
    2 使用 deirective ,自定义一个ng-onload事件
    *** 参考 http://stackoverflow.com/questions/17547917/angularjs-image-onload-event/17548090
    *** 参考 
      http://stackoverflow.com/questions/20863891
      /angularjs-directive-put-a-call-function-in-an-attribute-without-including-anot
    
## 20160219 获取图片大小部分代码转移到 services
    1 学习 factory promise
    2 学习 $q服务
    3 增加一个 sbLoad,不管用啊?
    4 怎么又回到了 directive 和services ?
    
## 20160219 网络监控提示去掉alert,改用 DOM

## 20160219 继续 图片上传 
    1 给照相机功能加上:上拉菜单 action sheet:1 现在拍照 2 从相册选取
    2 从ionic教程 copy 代码,记得引入 $ionicActionSheet,$timeout(设置为10秒)
    3 处理图片:裁剪\压缩!怎么知道确实裁剪了呢?
    4 增加一个显示图片对象主要属性的功能:文件大小,图片宽度和高度
    
## 20160219 在 testin 测试了该 ionic Demo
    1 android 4.1以下不要测试了,不支持! 安装不了
      5.1.1到6.0版本,没有安装失败的机器
    2 ios 6以下,还能找到这样的手机吗？
    
## 20160218 继续 图片上传 : 图片选取
    1 $cordovaCamera.getPicture(options) 的 sourceType 改成 从相册
      sourceType: Camera.PictureSourceType.SAVEDPHOTOALBUM,//.PHOTOLIBRARY, //.CAMERA,
    2 给 照相机 功能加上选择
      
## 20160217 关于 android 的file
            对象                      对应位置              属性
      applicationDirectory        file:///android_asset/  只读
      applicationStorageDirectory /data/data/<app-id>/    只读
      dataDirectory               files                   读写
      externalRootDirectory       <sdcard>/               读写
      externalDataDirectory       files                   读写  
          
    1 app自带的文件都是只读的,www的时候在data目录,android的时候全部转移到 assets,都是只读
      applicationStorageDirectory 也是只读,就是说:app带过来的数据都是只读的
    2 ios 的时候file结构再处理 http://ngcordova.com/docs/plugins/file/
    3 所以,下载的文件不能保存在 applicationDirectory
    4 照相的结果一定保存在 dataDirectory 或 externalRootDirectory..
       
## 20160217 图片上传
    1 复杂!先做图片选择,之后再来对付他
    
## 20160217 继续 图片下载 ,可惜这个功能没什么用途
    1 遇到错误 提示: EROFS (Read-only file system)
    2 参考 http://www.cnblogs.com/yshyee/p/4516662.html
    3 使用 cordova.file.documentsDirectory 得到 null
    4 使用 cordova.file.applicationDirectory,得到 URI not supported by CordovaResourceApi:
    5 只好 cordova.file.externalRootDirectory
    6 检验:打开文件管理,查看:本地,找到 testimage.png
    
## 20160217 继续 图片下载
    1 联机调试遇到跨域错误,提示:
      No Content-Security-Policy meta tag found. 
      Please add one when using the cordova-plugin-whitelist plugin.",
    2 白名单插件确实安装过
    3 在手机上运行,还需要修改 index.html,增加以下 meta
      <meta http-equiv="Content-Security-Policy"
                content="script-src * ‘unsafe-eval‘; connect-src * ‘unsafe-eval‘;
          object-src ‘self‘; style-src * ‘unsafe-inline‘; img-src *" >
          
## 20160217 编译遇到 ionic 过期了
    1 遇到提示 Ionic CLI is out of date:
    2 sudo npm install -g ionic 
    3 再次编译,遇到提示 建议升级苹果 Install ios-deploy to deploy iOS applications to devices. 
      sudo npm install -g ios-deploy
    4 遇到错误 ,以后再处理
      ENOENT: no such file or directory, 
      chmod '/usr/local/lib/node_modules/ios-deploy/build/Release/ios-deploy'

## 20160216 图片选择、上传
    1 cordova plugin add cordova-plugin-file-transfer
    2 从 ngcordova copy过来 fileTransfer.html
    3 从 ngcordova copy过来 starter.fileTransfer.ctrl,这次整体 copy 文件
    4 app.js 引入 'starter.fileTransfer.ctrl'
    5 index.html 中 加入 fileTransfer.ctrl.js
    
## 20160216 文件处理
    1 cordova plugin add https://github.com/pwlin/cordova-plugin-file-opener2.git
    2 从 ngcordova copy过来 fileOpener2.html
    3 从 ngcordova copy过来 starter.fileOpener2.ctrl,这次整体 copy 文件
    4 app.js 引入 'starter.file.ctrl'
    5 index.html 中 加入 fileOpener2.ctrl.js
    
## 20160216 设置android联机调试工具
    1 使用 android device monitor
    2 找到 android sdk 目录 ,tools 目录下的 monitor
    3 查看 logcat : 增加2个过滤器
      by log tag chromium
      by Aplication name com.ionicframework.starter
      
## 20160215 编译 android 错误
    1 错误提示 cordova/node_modules/q/q.js:126
    2 重新增加 android 平台 ionic platform add android
    3 重新安装模块依赖 sudo npm install
    4 重新编译,ok!怎么ok的?我也不知道
      sudo ionic prepare android
      sudo ionic compile android

## 20160215 tabs 会盖住 window 的一部分
    怎么让他独立?window 的内容减少tabs 的哪个部分?
    
## 20160215 文件处理
    1 从 ngcordova copy过来 file.html
    2 从 ngcordova copy过来 file.ctrl.js,这次整体 copy 文件
    3 吸取 ng-cordova 的模块命名规则,下一步修改整体架构
    4 app.js 引入 'starter.file.ctrl'
    5 index.html 中 加入 file.ctrl.js
    6 cordova plugin add cordova-plugin-file  
## 20160215 修改 login 错误
    1 close 的时候,清空 error 标志!不需要,只要在每次 login 时候,先清除 error 标志,其他地方不用清除      
    2 空,直接点 登录!简单按照长度不够提示,细化:不允许空待处理
    
## 20160215 init.sh
    1 从 ngcordova copy过来 init.sh
    2 待以后完成demo时,删除不需要的 plgin
    
## 20160215 网络处理   
    1 从 ngcordova copy过来 network.html
    2 从 ngcordova copy过来 NetworkCtrl
      前天已经做过
    3 列举了 wifi ,4g,其他待测试
    4 network.html的 refresh() 没有反应
    
## 20160214 网络处理
    1 cordova plugin add cordova-plugin-network-information
    2 ng-cordova在监控 networks 状态变化时,会给rootscope发送广播
      程序需要在收到网络变化时进行相关处理,这里仅仅简单通知
      只在网络发生变化时才会触发,测试方法:开启\关闭飞行
      
    3 在 app.js 接收广播事件即可,单独查看网络状况没有意义
    4 列举网络连接资源?
    5 怎么监控网络信号强弱?
    
## 20160213 学习 ng-cordova
    1 模块结构分离,他的 directive,services 怎么定义呢？
    2 init.sh 这个批处理方法好!每增加一个plugin,就写进去,省的以后一个一个的找
    3 karma gulp 待学习
    4 获取手机号 imei不在cordova插件中,需要自行改造
## 20160212 配置 gitosc
    不行!还是不行!以后再学习
    
## 20160212 获取手机号、设备号
    1 从 ngcordova copy过来 network.html
    2 从 ngcordova copy过来 NetworkCtrl
    3 不对!那是一个listen事件,监控网络状况,待处理
    4 从 ngcordova copy过来 device.html
    5 从 ngcordova copy过来 DeviceCtrl
    6 cordova plugin add cordova-plugin-device
    7 手机号?待处理
    8 怎么能够不出现 html?那么,不能直接href了!待修改
    
## 20160211 尝试 $inject
    1 确实应该那么做,本次不进行了,改动量太多
    2 下一版本 controller directive services 分别不同目录,模块最小化之后,一个文件只有一个组件
    3 同时完成 $inject
    
## 20160210 阅读 Angular代码规范
    1 http://www.reqianduan.com/1722.html
    2 手动依赖注入 用$inject手动添加Angular组件所需的依赖
    3 范例 
      /* recommended */
      angular
        .module('app')
        .controller('Dashboard', Dashboard);
      
      Dashboard.$inject = ['$location', '$routeParams', 'common', 'dataservice'];
      
      function Dashboard($location, $routeParams, common, dataservice) {
    4 下次试试
    
## 20160202 学习 gulp 不成功,下次继续
    1 npm install -g gulp
    2 npm install gulp --save-dev
    3 npm install gulp-jshint gulp-sass gulp-concat gulp-uglify gulp-rename --save-dev

## 20160202 代码整理
    1 将 app 的常量集中到一起
    2 给 appctrl 减肥,该谁的ctrl移动到谁,能 servcie 的改到 service
    3 下一步 手势处理
    4 编译时,不要修改任何代码,包括 md 
    
## 20160202 照相机功能
    1 插件总是容易忘记或者搞错,加上 plugin 是否已经安装,已经引入 的判断
    2 如果需要在图片框显示,就不能存到相册?为什么?那么怎么保存呢? 
      saveToPhotoAlbum: false,
    3 为了在手机能看清,图片框只设置了 150,剪切大小也只是设置了 200
      根据需要自行调整擦描述
    4 保存到相册 也许是需要 file 支持,等做完图片上传之后,回头对付这个
      
## 20160202 照相机功能
    1 添加插件 cordova plugin add cordova-plugin-camera
      ** 或者 cordova plugin add https://github.com/apache/cordova-plugin-camera.git
      ** 删除插件 cordova plugin remove cordova-plugin-camera
    2 疑问: 如果同一个功能,在不同地方使用,怎么办?ctrl 只能在ctrl?
    3 怎么没有反应?二维码扫描也不稳定?
    
## 20160201 二维码后续
    1 http://ngcordova.com/docs/plugins/barcodeScanner/ 官网找文档
    2 下载了原文,没有发现 scan 方法有控制选项,待以后再说,看来他是条码最合适
    3 发现了 encode 方法生成二维码,下次试试看
    
## 20160201 11版本不认识 md文件了
    1、在Webstorm中选择: File –> Settings –> Plugins –> Browse repositories –> 搜索markdown
    2、查看文件类型的绑定：File –> Settings –> File Types –> Markdown files
    
## 20160131 完善工具,优化代码
    1 inspect code 重点:不对称的tag,重复定义,缺少定义
    2 升级 WS 到11.3
    3 GitOSC 代码测试,确认技术债务非我的代码
    
## 20160131 $cordovaBarcodeScanner 学习
    准备加上照相功能、上传功能

## 20160131 测试
    1、生成apk测试
    2、扫描二维码实际扫描认识的事条码，什么地方设置的？待处理！正好也加上扫描条码功能
    3、退出qq浏览器重新运行，扫描条码、二维码都能认识
    4、取景框长方形，怎么改成正方形？
    5、跳转到jq 的页面，都要加上 回退 btn，不能指望大家都是用的 android，有硬回退
      setting 的 跳转到 jq 的二维码
      menusetting 中的3个二维码 btn 都不反应，改错了？
      menusetting 中跳转到 jq ，也要加上 回退
    6、sidemenu 太宽
    7、扫描二维码 取消操作也弹出提示？
    8、登录的关闭 close 改成中文
    9、登录 没有标题，加上
    10、登录的错误提示详细一些
    11、登录加上 注册、忘记 btn
    
## 20160130 cordova 插件处理
    1、浏览器下能否支持 cordova
    2、扫描二维码在浏览器能不能？是否需要 cordova 以及 照相插件？
    
## 20160130 扫描二维码
    1、参考 http://www.wojilu.com/space/author/blog/post/828
    2、下载并 ng-cordova.min.js 拷贝到项目的 www/js 目录 
    3、添加插件 cordova plugin add https://github.com/wildabeast/BarcodeScanner.git
      ** ?? 或者 phonegap plugin add phonegap-plugin-barcodescanner
      cordova plugin add phonegap-plugin-barcodescanner
      ** 官网上写的是git,是否不一样?
      
    4、修改 index.html ，增加 ng-cordova.js
    5、修改 app.js ，注入 ngCordova
    6、修改 controllse.js 的 appCtrl，在根 ctrl 下加入 scanBarcode 功能函数
    7、在 setting.html 增加测试扫描二维码功能的 btn，调用 ng-click="scanBarcode()
    8、增加 android，预编译、编译后，下载到手机测试
    9、不生成 app，在浏览器测试不成功！人说错了？
    
## 20160129 login 完善
    1、login 验证输入、验证登录放到 service 
    2、给 login 加上简单输入规则验证 
    3、放弃之前的一个客户端主动设置超时事件，保留 http.get 的超时限制
    4、login 就到这里，以后进一步完善
    5、顺手： href 换成 ng-ref,参看 http://www.cnblogs.com/zhoujg/p/4908049.html
    
## 20160129 重新理解 vm 、 ctrl 
    1、 popup的使用有问题
    2、dologin 已经那个改制处理 http，返回结果
    3、vm 的操作应该在 dologin 的外面完成
    4、返回直接使用 $ionicGoBack()，无需在使用 goback123() 等控制
    5、给 appCtrl 减肥
    6、login 放回到 appCtrl，因为他属于主页，也就无需发送广播了？   
    7、总弹出提示：是否保存密码，这个需要 cookie 处理，下次再处理
    
## 20160128 发布 android 测试
    1、首页加载的进度条在左边
    2、登录后可以显示人名，但是下次进来不行，没有LS
    3、menuSetting 那些button 只能2个字，太少了
    4、跳转 jq 的那个WX页面，回不来，因为他是 0%
    5、条码太大了
    6、sidemenu 占地太宽
    
## 20160128 用户名中文，有问题
    麻烦！一旦中文后，只能先去掉 run 的初始化 Globals.mLoginData 才能恢复！
    1、不是汉字造成的！是 dologin 的时候， loginData 还是 空！
    2、popup 的 destroy 事件有问题
    
## 20160128 清空LS，需要发送广播通知大家，否则返回是那些参数、状态还在呢！
    ok!

## 20160128 app 运行加上初始化，主要读取 login 状态
    1、配置全局公共变量
    *** 可以变化的：变量 
      方法： phonecatApp.value('test',{"test":"test222","test1":"test111"});  
    *** 不可以变化的 常量，产生的常量不可以被装饰器拦截  
      方法： phonecatApp.constant('constanttest', 'this is constanttest'); 
    *** 通常用value()来注册服务对象或者函数，而用constant()来配置数据
    2、在需要的 scope 的地方，赋值个本地 scope 的变量,以便绑定事件
    3、在 menuSetting 加上一个 clear LS ，以便测试 LS 相关功能
    4、我决定使用一个服务来共享本 app 的公共变量，需要用到的地方，记得： 注入 Globals
    *** 这种方式符合 ng 的 依赖注入 思想
    *** 服务器url,API，数据库连接，登录名、密码 等等
    *** 全局变量名以m开头，例如：mPlayLists,mLoginData
    5、用户已经登陆，用户名点击先简单做一个 logout
    
## 20160127 判断设备、网络 popup 显示
    1、现在开始，README.md 在 www目录下
    2、增加一个 CheckNavigator 检测浏览器参数的 service
    3、现在开始，增加的功能主要放在 menuSetting.html
    

## 20160127 login 独立 ctrl，共享数据
    1、使用广播来发送数据和消息，登录之后发送一个广播
    2、appCtrl 放一个接收器。用来刷新 loginData
      *** 务必注意 $scope.$on 这个 on 前面的 $,代表内部事件、数据...
    3、login 转移到 directive 留待以后
    4、只有登陆成功才会写入LS，转移到 app 完成
      
## 20160127 login 转移到 directive
    1、将 login 部分 UI 连同 dologin 转移到一个 directive
    2、其中的 loginData 采用双向绑定，这样，directive中 改变登录用户或状态也会同样改变 appctrl 中的 loginData
    3、其中加上一个 dologin 方法
    4、这个方法怎么更难，放弃
    
## 20160126 login 转移到 service
    1、登录后隐藏图标按钮，显示文字按钮（增加一个）
    ** 有没有 button group ?以后再说
    2、首先将 login 的 ctrl 从 AppCtrl 独立出来，建立自己的 LoginCtrl
    3、现在需要解决不同 ctrl 之间的数据共享了！
    4、又回到 $scope 作用域的问题了！
    5、先解决模拟 API 路径问题，改到本地 data 目录
    
## 20160125 login 成功之后处理
    1，首页右上角因该显示的事：用户名
    2、点击 用户名 的地方应该是 logout?应该是 我 的信息！在其中包括：logout
    3、增加一个 已经登录 标记，并且使用该标记控制 隐藏用户图标
      ** var x = {"hasLogined":"true"};
    4、合并输入的 login 信息，服务器返回的信息，以及 已经登录 
      ** angular.extend(x, $scope.loginData, response.data)
    5、合并之后的状态和信息保存到 LS,下次 run 的时候，init 读取 LS就能知道他是否已经登陆
      
## 20160124 处理 doLogin，加上一个进度条，加上超时错误处理
    1、需要引入 http,localStorageService
    2、需要一个 USER_API_URL
      ** 加上一个 data 目录。data 目录下放一个 Customers_JSON.php 用于模拟 api
      ** 将该 php 文件放到本地 web 服务器的 angular/data 目录
    3、加上一个进度条 loading，记得 给他的 ctrl 加上 $ionicLoading
      ** 注释处留下 5 种进度条效果
    4、超时怎么办？重新登录？这里就不处理了，
      ** 将 USER_API_URL 改成一个找不到的网址 ，例如： 127.0.0.111
    5、log记录date方式改成可以识别的样子
      ** 直接 new 一个 ： new Date().toLocaleString()
      ** 返回的结果： "2016年1月25日 GMT+8下午2:46:41" 
      
## 20160122 本地存储 Local Storage 处理


## 20160122 继续处理 login
    1、定义一个数据结构 loginData 
    2、怎么记住已经登陆过的用户名、密码？需要 cookie处理？
    3、如何调用 api 到服务器验证？api 期间加载一个 loading 动画进度条？
    4、登录错误怎么处理？登录超时怎么处理？
    5、登陆成功本地 cookie 保存怎么处理？
    6、安全做法是 token 方式，待以后学习
    7、先试用 Local Storage 方式做一个
    8、参考 http://blog.csdn.net/linlzk/article/details/45536065
    
## 20160122 继续处理 login，学习使用 Modal
    1、login的 modal 单出来一个，不要放在根下
    2、先在 button 测试的 menuSetting.html中单做 login123()
    3、什么时候destroy modal ? 监控事件一个都捕捉不到，以后再处理
    
## 20160121 处理 login，优化处理
    1、跳转到 jq 页面只能 href 方式，总有不兼容之处，继续
    2、去掉 side menu 和 tabs 的测试 jquery，放到menuSetting 中的 button
    3、tabs 图文同事显示，文字在下
## 20160120 努力 自定义的 directive
    1、总算做出来一个二维码的，简单的，单向的
      自定义一个 barcode 标签/属性，需要注入4个参数
       1)、text
       2)、width
       3)、height
       4)、图片
     2、text 改成双向绑定，参数 text 实际值从他的ctroller中继承、获取
     3、directive 中判断引用方是 A 还是 E ，获取尺寸方式不同
     4、加 logo 太费劲！待处理
     5、directive 待下次进一步
        1）、&方式：执行外部方法
        2）、二维码家logo
        3）、加上 templateUrl
        4）、在 popup/modal中弹出
        5）、自动居中
     
## 20160118 将 jq 做成的二维码方式，封装成 ng 自定义的 directive
    1、太难了！先学习一个 ng 封装 ng 的
    *** 使用@（@attr）来进行单向文本（字符串）绑定
        DOM 元素绑定到某个变量值，变量值有 controller 设置
        外部只管引用，该指令的属性值等data都由自定义指令自己完成
    *** 使用=（=attr）进行双向绑定
        DOM 元素绑定到某个变量值，指令中某个变量被绑定到DOM，两者可以互相对应，互相联动？
        一般用于 input ，改变任何一个input都会改变另一个值
    *** 使用&来调用父作用域中的函数
        自定义指令中的函数可以不用再写，直接调用父亲即可
        
## 20160117 二维码
    1、在 lib 建立一个新目录 common ,将 qrcode 放这里，以后公用第三方也放这里
    
## 20160117 
    1、menuSetting 去掉那些使用 onclick 的，一律使用 ng-clcik 回退还不行，先保留 
    2、goHome,goBack,Login等全部改成 ng-click 事件，去掉以前使用的 href 方式
    3、给 btton 加上 styles ，大量测试以后从这里开始，摆放好点儿
    
## 20160116 App应用名字+应用图标+启动画的修改
    *** 不要手工修改 platforms 目录下的内容，那些都是自动生成的
    *** 除了这个之外： \platforms\android”下面新建一个release-signing.properties
          内容如下
          *****
          key.store=/Users/dhbm/DeskTop/Android/dhbm.keystore
          key.alias=dhbm.keystore
          key.alias.password=www.dhbm.cn
          key.store.password=www.dhbm.cn
          *****
          cordova build android --release
          生成的 apk位置：
          /Users/dhbm/Desktop/pg2015122401/platforms/android/build/outputs/apk/android-release.apk
    1、参考 http://jingyan.baidu.com/article/c35dbcb0ec59438916fcbcaf.html
    ### 修改config.xml(假设创建项目时，没有加上包名等参数)
      包名：widget id="com.dhbm.demo123"
      版本号：version="0.0.1"
      程序名：<name>IonicDemo</name>
      程序简介：<description> IonicDemo by wzh 20160116 </description>
    2、怎么直接设置编译出来的 apk 文件名？（不是上面说的 程序名、app名）待处理
      cordova compile  -help 没有发现有这个选项，就只好每次都 rename，当然只在 release 的时候才真的需要
    3、程序图标：resources 目录下 android 目录 icon 目录
    4、启动画面 splash : resources 目录下 android 目录 splash 目录
    
## 20160116 android打包测试--debug
    1、增加 platforms,预编译、编译
          ionic platforms add android
          ionic prepare android
          ionic compile android
    2、运行结果，列表数据不出来，说明无法访问外网(404)
    3、增加白名单
    cordova plugin add cordova-plugin-whitelist
    ionic plugin add cordova-plugin-whitelist
    4、在 index.html 首页增加 meta项， 设置安全性
    <meta http-equiv="Content-Security-Policy"
          content="script-src * ‘unsafe-eval‘; connect-src * ‘unsafe-eval‘; 
    object-src ‘self‘; style-src * ‘unsafe-inline‘; img-src *" >
      以上 meta 加上之后会报错，难道我copy错了？但是测试发现，不用加这个meta
      
    5、ion-infinite-scroll有bug，
      将services中，首次读取数据改成15条，保证超出一屏所能显示的条数
      将每次上拉刷新的条数也改成10条
    6、修改过 html之后，不用重新增加 platforms，直接预编译、编译
    7、列表ok，点击详细ok，幻灯ok！
    8、修改 sliders.html 之后，预编译发现，platforms 中对应的  sliders.html 也变化了！
      原来预编译干这个的！
    9、测试jQuery 页面，那个WX年代就停留在 0%，为什么呢？
    
## 20160115 加上上拉刷新（走到最下面）
    1、menu.html 最后加上 <ion-infinite-scroll>
    2、PlaylistsCtrl 加上 loadMoreData（）
      这次 加载更多，这次往后追加数据（上拉是插入到前面）
    3、给 <ion-infinite-scroll> 加上 ng-if 条件，防止无限刷新
      根本停不下来啊！待处理
    4、20160116才发现多了一个<ion-infinite-scroll>
    
## 20160114 测试 jQuery 页面 混合
    1、menu.html 中 href="#/app/testJquery" 和 
    使用了 ionic view 渲染，会出现ion-header-bar并带着 回退，带着 title 内容是： 测试jquery页面 
    *** sidemenu 中点击，则左上角还是 sidemenu 图标
    *** 底下 tabs 中点击的，则左上角是 返回
    *** 从sidemenu 中 ”按钮、iframe、回退“进去 menuSetting中的 "测试jQuery页面"，则左上角是 返回
    2、href="./test20160112.html" 效果是不同的
    完全是 test20160112.html 的页面 ，没有回退和title

## 20160114 幻灯数据动态加入
    1、动态数据可以通过 http get获取后，使用$ionicSlideBoxDelegate
      我这里先暂时使用了全局变量
    2、html文件中，ion-slide-box 的关键属性 delegate-handle 用于标识本 slide 
      $ionicSlideBoxDelegate.$getByHandle('slideImgs').update();
    3、幻灯列表的每一项 ion-slide，使用 ng-repeat,循环数组获取在他的控制器 SlidersCtrl 完成
      如果是 http get ，可以讲 get 方法封装到 services ，这里直接调用
    4、does-continue 表示到尾巴之后，从头再循环
    5、使用了图片响应式  class="img-responsive",保证 width 100%，在电脑上看着太宽，但是在手机上就合适了
      电脑上水平居中，手机上，垂直居中，待处理
    
## 20160112 加上 幻灯 演示
    1、怎么将 styles 内容转移到 我们自己的 css文件
    2、幻灯的左右滑动和sidemenu冲突了，怎么办？
      幻灯应该属于某一个页面的下一级子页面，本来左上角就没有 sidemenu，只有 返回，所以先忽略
    
## 20160112 在 右上角 加上 登陆/注册
    1、修改 menu.html 在 ion-nav-bar 右边放一个 button 图标 ion-android-person
    2、加上 ng-click ng-click="login()"
    3、去掉其他地方的 登陆/注册，主页改成首页
    4、不会了，待处理：menu.html里面，tab 部分我是希望文字在图标下方，sidemenu部分，我是希望图标大点儿，文字靠右边
  
## 20160109 从 app.js 分离出 router.js
    1、新建一个 routes.js
    2、新建一个 starter.routes 模块，并且注入 对应的参数
      angular.module('starter.routes', ['ionic', 'starter.controllers' ,'starter.services'])
    3、从 app.js 中 转移所有的 config 代码到 routes.js
    4、修改 app.js ，给 starter 模块 加上 starter.routes 注入，如下第二个参数
      angular.module('starter', ['ionic', 'starter.routes', 'starter.controllers' ,'starter.services'])
    5、以后 增加所有的 UI 和 url ，在 routes.js 增加对应的路由
    6、app.js 只存放 全局变量，app初始化需要的 data
    
## 20160109 获取明细信息，打开明细页面
    1、先直接用一个 全局变量 来表示
      var mGlobalPlayLists = [];
      实际环境时，应该是从 localstorahe 或者 indexDB 获取
    2、playlists.html 中点击明细 href 改成 ng-click="getThisPlayer123(playlist)"
      这样子可以传递正哥 playlist 对象，而不是某一个值 
    3、getThisPlayer123(playlist) 需要在 AppCtrl 里 $state.go 处理跳转
      在这里重新获取传递过来的对象中的任何参数
      这里只需要一个 "playlistId": playlist.user.salt  
    4、playlist 使用大图 large ，playlists 使用中图 medium，缩略图暂时没有使用
  
## 20160109 加上 下拉刷新
    1、 怎么添加数据？
      将这种公用功能封装到services，注意参数 引入 $http
        .factory('NewPlayers', function( $http)
      注意 peomise 的写法，参数是在then 里面的 func里面
        .then( function(response)
    2、怎么调用 services 里面的 func
      对应的 controller 中，注入需要使用的 services 的名字 ： NewPlayers
        .controller('PlaylistsCtrl', function($scope,NewPlayers)
    3、怎么停止刷新？
      在promise之后，发送一个广播 
      如果是 ajax ，在 finally 发送一个广播 
        $scope.$broadcast('scroll.refreshComplete');
    4、http返回对象 object 的json处理
      参照 services.js代码
    
## 20160109 加上 list 和refresher
    1、修改 list，重点是 item-avatar 样式（带图片的列表）
      item-avatar 在demo的tabs中有样本（我这里是20151225）
    2、见 playlists.html 注释
    3、刷新的 services 的 promise没有搞定 
    
## 20160107 试试混合页面
    1、单独建立一个 jq 目录
    2、copy 水 代码全部到jq目录
    3、menu.html 的 setting 的 href="#/app/playlists
    4、修改 menuSetting.html，参看他里面的注释
    5、给 menuSetting.html 里面的 button 增加点 响应
       onclick和ng-click区别，href,goback()处理
      
## 如何 copy 新项目
    1、到 文件夹，直接 copy 出来一个新的文件夹
    2、open 新的文件夹
    3、点击 该目录(项目)，右键 rename
    4、选择 rename project
  
## 20160106 努力组合 sidemenu + tabs 
    1、创建一个 sidemenu 空白项目
     ionic start ion2016010601 sidemenu
    2、打开 templates 目录下的menu,html,在后面增加一个div tabs
      <div class="tabs tabs-icon-only">
    3、tabs特意增加到5项,菜单也对应增加一项
      tabs 和 sidemenu 是一样的，因为 app.js 配置的 state 一样
      如果 state不一样，对应的url也就不一样
      即使完全一样，从 menu 进去的，左上角还是带着 sidemenu，从 tabs 进去的，就不带，而是带有 返回 按钮
    4、app.js 增加 setting 的状态机
    5、controller.js 增加 setting 的 controller ，即 BarcodeCtrl
    6、这个布局到此为止
    7、试图从 tabs 项目做成同样效果，不成功！
      
##  努力组合 sidemenu + tabs 
    1、创建一个 blank 空白项目
     ionic start ion20160106 blank
    2、创建一个templates目录
    3、从 sidemenu 项目 copy 过来所有模板，
    4、修改 index.html,加上 
    <ion-nav-view></ion-nav-view>
    5、先做成了一个 sidemenu 项目
    6、修改 menu.html ，然后。。。。。，参看后续直接从 sidemenu 项目修改

##  20160105 关于ionic 图标 如何从 ionic 自带图标选择
    1、http://ionicons.com/ 
    2、在这个页面看中哪个，点击后提示他的名字，copy过来
    3、图标成对出现 带 -outline 的作为没有点击的状态，不带 -outline 的最为点击选中的状态
## 自定义 ionic 图标
    1、百度 ionic 图标 自定义 ，找个方法创建、导入、引用
    2、待处理
 
## 20151230 tabs + sidemenu 的
### 这是一个 blank 空白项目
    创建方式 $ ionic start ion20151230 blank
### 对比tabs,sidemenu项目，区别如下：
    1、www目录没有创建 template 模板目录
    2、主页 index.html 的body 部分只有一个简单的 panel，没有 ion-nav-view 的UI-Router
    3、主程序 app.js 中，只有app的controller(这里叫做starter),并且没有任何注入参数
        ----因此，这里没有 controller.js,来扩展 starter.controllers
        ----没有初始化的残路，例如：sidemenu项目中的 playlists 和 loginData 以及 login 对应的 $ionicModal
        ----主页UI没有任何 href或者button等，也就不需要定义那些对象的controller，例如：tabs项目中的 ChatsCtrl、ChatDetailCtrl
    4、没有状态机路由 $stateProvider
    5、没有 url 路由 $urlRouterProvider，所以主页就是 index.html里面的那个 panel，不会调准到某个模板去
    6、没有自定义任何指令，例如：tabs中的sevices里面的factory方式的 chats

## 20151224 创建应用程序 
    1、我这里直接使用Webstrom创建了
    2、CLI方法 cordova create hello com.example.hello HelloWorld 
  
# 必须预先进行的安装
    1、安装 nodejs 
    2、安装JAVA
    3、安装 android sdk tools
    4、安装ANT
      这个不需要了！
    5、安装 Xcode
    
## 查看 node 安装路径    
    npm root
## 升级到稳定版本   
    n stable   
## 查看 node 版本
    node -v
## 查看 npm 版本
    npm -v  
## 查看 ionic 版本
    ionic -v
## 查看 cordova 版本
    cordova -v

# 将整个目录权限都改成 可读写（mac必须root权限）

## 在项目目录下运行 操作 
### 安装package.json中的依赖的js模块
    npm install
  
# 增加运行平台
    ionic platform add android
    ionic platform add ios
    ** githubs 需要翻墙
    ** 有时需要2-3次才能完成

  
## 编译应用程序 ：如果build一步不能正确，那么分两步完成 (直接使用 build总是不能完整)
   1、 ionic build android
    等效于：
      cordova prepare android
      cordova compile android
    
      cordova prepare android --release
      cordova compile android --release
    2、ionic build ios
    等效于：
      cordova prepare ios
      cordova compile ios
 
## www运行
    直接打开 index.html，在浏览器运行

## android 打包、运行
    *** 不要修改platforms下面的www目录，自动生成的
    *** 再次将整个目录权限都改成 可读写（mac必须root权限），因为这次又自动生成了android目录
    
    ### 使用以上增加平台、编译生成
      主文件在 ：MainActivity.java
      debug apk文件在 ../platforms/android/build/outputs/apk/android-debug.apk
      未签名 apk 位置
      ../platforms/android/build/outputs/apk/android-release-unsigned.apk
    参考 http://rensanning.iteye.com/blog/2030516
  
    ### 最终方案 ： \platforms\android”下面新建一个release-signing.properties
      内容如下
      *****
      key.store=/Users/dhbm/DeskTop/Android/dhbm.keystore
      key.alias=dhbm.keystore
      key.alias.password=www.dhbm.cn
      key.store.password=www.dhbm.cn
      *****
      cordova build android --release
      生成的 apk位置：
      /Users/dhbm/Desktop/pg2015122401/platforms/android/build/outputs/apk/android-release.apk

    参考 http://www.tuicool.com/articles/eEj2Q3
    Proguard 会混淆cordova及其插件的java代码，导致apk运行时报 cordova error initial class，
    解决办法是在proguard-project.txt 加入下面的内容，不混淆cordova及其插件
    -keep class org.apache.cordova.** { *; }
    -keep public class * extends org.apache.cordova.CordovaPlugin
    
    参考 http://lzw.me/a/cordova-3-5-android-apk-signed.html
    文件 ant.properties 怎么不行？待处理
  
## iOS 打包 发布
    *** 不要修改platforms下面的www目录，自动生成的
    *** 最好将整个目录权限都改成 可读写（mac必须root权限）
    
    使用以上增加平台
    直接生成ipa以及导入到xcode，待处理
  
## 关于windows版本的WS
    *** 不会自动产生 platforms 目录，请在terminal下cli方式增加
    ### 增加3种平台方法
      sudo ionic platforms add android
      sudo ionic platforms add ios
      sudo ionic platforms add browser


  