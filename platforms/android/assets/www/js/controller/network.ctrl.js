angular.module('starter.network.ctrl', [])
//add by wzh 20160212
.controller('NetworkCtrl', function ($scope, $cordovaNetwork,MsgBox) {
  var init = function () {
    $scope.networkType = null;
    $scope.connectionType = null;

    document.addEventListener("deviceready", function () {
      $scope.networkType = $cordovaNetwork.getNetwork();

      if ($cordovaNetwork.isOnline()) {
        $scope.connectionType = 'Online';
      }
      else if ($cordovaNetwork.isOffline()) {
        $scope.connectionType = 'Offline';
      }
      else {
        $scope.errorMsg = 'Error getting isOffline / isOnline methods';
      }
    }, false);

  };

  if (!window.cordova) {
    MsgBox.show("这个功能只能在手机使用.....");
  }
  else {
    init();
  }


});
