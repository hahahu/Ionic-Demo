angular.module('starter.fileOpener2.ctrl', [])
  .controller('FileOpener2Ctrl', function ($scope, $log, MsgBox, $cordovaFileOpener2) {
    /* 不能读取 预先打包进来的文件,那是元数据,读取方式不一样
    $scope.packageId = 'data/test.apk';
    $scope.type = 'application/vnd.android.package-archive';
    $scope.logs = '';
*/
    var sss = "<b><h2> 这个功能只能在手机使用</h2></b>";
    if (!window.cordova) {
      MsgBox.show(sss);
      return;
    }

    var fileDir = cordova.file.applicationDirectory ;  //这个对应assets 只读
    //var fileDir = cordova.file.dataDirectory ;

    var fileName = "www/data/test.apk";
    fileDir = fileDir + fileName;

    $scope.packageId = fileDir;
    $scope.type = 'application/vnd.android.package-archive';
    $scope.logs = "";
    console.log('$scope.packageId 111 === ' + $scope.packageId);

    $scope.open = function () {
      console.log('$scope.packageId 222 === ' + $scope.packageId);
      $cordovaFileOpener2.open(
        $scope.packageId,
        $scope.type
      ).then(
        function () {
          $scope.logs = 'Success';
        },
        function (error) {
          console.log('error code: ' + error.code + ' , message: ' + error.message);
          alert('error code: ' + error.code + ' , message: ' + error.message);
          $scope.logs = 'error code: ' + error.code + ' , message: ' + error.message;
        }
      );
    };

  });
