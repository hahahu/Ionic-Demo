angular.module('starter.localNotification.ctrl', [])

  .controller('LocalNotificationCtrl', function ($scope, $rootScope, $cordovaLocalNotification,MsgBox) {
    var sss = "<b><h2> 这个功能只能在手机使用</h2></b>";
    if (!window.cordova) {
      MsgBox.show(sss);
      return;
    }

    $scope.addNotification = function () {
      var now = new Date();
      var _60_seconds_from_now = new Date(now + 60 * 1000);
      var event = {
        id: 1,
        at: _60_seconds_from_now,
        title: "测试消息Title",
        text: "测试消息之内容:很高兴见到你!祝你好运!"
      };

      document.addEventListener("deviceready", function () {
        $cordovaLocalNotification.schedule(event).then(function () {
          console.log("local add : success 恭喜你,本地消息发送成功!");
        });

      }, false);

    };

    document.addEventListener("deviceready", function () {
      $rootScope.$on("$cordovaLocalNotification:trigger", function (event, notification, state) {
        console.log("notification id:" + notification.id + " state: " + state);
        console.log("点击消息之后会走到这里!请继续......");
      });
    }, false);
  });

