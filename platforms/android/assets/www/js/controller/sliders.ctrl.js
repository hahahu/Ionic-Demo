angular.module('starter.slider.ctrl', [])
  .controller('SlidersCtrl', function ($scope, Globals, $ionicSlideBoxDelegate) {
    //指定缺省的第一个，如果改成点哪个就是那个，待处理
    $scope.myActiveSlide = 1;
    //这里使用了全局变量直接获取，不用 promise，如果改成 http get。待处理
    //只截取前 10 个.演示效果而已
    //add by wzh 20160616 网络不好,返回 null时,这里也是空,先简单返回
    if (Globals.mPlayLists == null || typeof(Globals.mPlayLists.length) == 'undefined' || Globals.mPlayLists.length < 10) {
      console.log("空空如也!111");
      return;
    }
    console.log("空空如也!222");
    console.log(Globals.mPlayLists);

    $scope.playlists = Globals.mPlayLists.slice(0, 10);

    //console.log($scope.playlists[0].user.username);
    //console.log($scope.playlists[0].user.picture.medium);
    //console.log($scope.playlists[0].user.location.city);

    //如果update有问题，再加上试试 $ionicSlideBoxDelegate.update();
    $ionicSlideBoxDelegate.$getByHandle('slideImgs').update();

    $scope.slideHasChanged = function ($index) {
      console.log("slideHasChanged $index >>>>>>" + $index);
    }

  });
