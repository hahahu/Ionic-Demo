angular.module('starter.chat.ctrl', [])
  //这是本app的主控
  //三个参数 'socket','randomColor','userService' 分别定义了对象，都是全局的？不属于 $scope ，而是属于 $rootScope
  .controller("ChatCtrl", ['$scope', 'socket', 'randomColor',
    'userService', 'Globals', 'MsgBox', 'localStorageService',
    'messageService', '$state',
    function ($scope, socket, randomColor,
              userService, Globals, MsgBox, localStorageService,
              messageService, $state) {
      var messageWrapper = $('.message-wrapper');
      console.log("chatCtrl >>>>>>");

      $scope.messages = messageService.getAllMessages();
      console.log($scope.messages);

      $scope.onSwipeLeft = function () {
        $state.go("tab.friends");
      };
      $scope.popupMessageOpthins = function (message) {
        $scope.popup.index = $scope.messages.indexOf(message);
        $scope.popup.optionsPopup = $ionicPopup.show({
          templateUrl: "templates/popup.html",
          scope: $scope,
        });
        $scope.popup.isPopup = true;
      };

      //标记 已读/未读
      $scope.markMessage = function () {
        var index = $scope.popup.index;
        var message = $scope.messages[index];
        if (message.showHints) {
          message.showHints = false;
          message.noReadMessages = 0;
        } else {
          message.showHints = true;
          message.noReadMessages = 1;
        }
        $scope.popup.optionsPopup.close();
        $scope.popup.isPopup = false;
        messageService.updateMessage(message);
      };

      $scope.deleteMessage = function () {
        var index = $scope.popup.index;
        var message = $scope.messages[index];
        $scope.messages.splice(index, 1);
        $scope.popup.optionsPopup.close();
        $scope.popup.isPopup = false;
        messageService.deleteMessageId(message.id);
        messageService.clearMessage(message);
      };

      //这里是新消息?
      $scope.topMessage = function () {
        var index = $scope.popup.index;
        var message = $scope.messages[index];
        if (message.isTop) {
          message.isTop = 0;
        } else {
          message.isTop = new Date().getTime();
        }
        $scope.popup.optionsPopup.close();
        $scope.popup.isPopup = false;
        messageService.updateMessage(message);
      };

      $scope.chatDetils = function (message) {
        console.log('chatDetails chatDetails chatDetails');
        console.log('message.id ===' + message.id);
        $state.go("chatDetail", {
          "messageId": message.id
        });
      };

      $scope.$on("$ionicView.beforeEnter", function () {
        /* 这个在点击进入聊天详细才需要
        //全局变量 mLoginData 会在app run的时候初始化
        var loginData = Globals.mLoginData;
        console.log("chatCtrl >>>>>>" + loginData);

        $scope.nickname = loginData.username;
        $scope.hasLogined = loginData.hasLogined;

        if (!loginData.hasLogined) {
          var sss = "<b><h2> 请先登录...</h2></b>";
          MsgBox.show(sss);
          return;
        }
        else {
          console.log("addUser >>>>>>" + $scope.nickname);
          socket.emit("addUser", {nickname: $scope.nickname, color: $scope.color});
        }
        */
        // console.log($scope.messages);
        $scope.messages = messageService.getAllMessages();

        $scope.popup = {
          isPopup: false,
          index: 0
        };

      });


      // $scope.hasLogined = false;
      $scope.receiver = "";//默认是群聊
      $scope.publicMessages = [];//群聊消息
      $scope.privateMessages = {};//私信消息
      $scope.messages = $scope.publicMessages;//默认显示群聊
      $scope.users = [];//
      $scope.color = randomColor.newColor();//当前用户头像颜色

      /* //登录单出去了
       $scope.login = function () {   //登录进入聊天室
       //$scope.nickname = "wzh001";
       console.log("addUser >>>>>>" + $scope.nickname);
       socket.emit("addUser", {nickname: $scope.nickname, color: $scope.color});
       }
       $scope.scrollToBottom = function () {
       messageWrapper.scrollTop(messageWrapper[0].scrollHeight);
       }
       */


      //这是 点击 发送消息 的处理，向服务器发送 addMessage ，
      //服务器没有给 我(发送方) 返回一个 状态报告？是不是socket.js里面处理了？，
      $scope.postMessage = function () {
        // $scope.words = "哈哈哈哈哈哈12345";

        var msg = {text: $scope.words, type: "normal", color: $scope.color, from: $scope.nickname, to: $scope.receiver};
        var rec = $scope.receiver;
        if (rec) {  //私信
          if (!$scope.privateMessages[rec]) {
            $scope.privateMessages[rec] = [];
          }
          $scope.privateMessages[rec].push(msg);
        } else { //群聊
          $scope.publicMessages.push(msg);
        }
        $scope.words = "";//清空底部输入的内容？是不是应该放在emit之后？待处理
        if (rec !== $scope.nickname) { //排除给自己发的情况
          //自己发给自己只在本地显示，不要发送到服务端，当然如果系统要求，可以放开
          socket.emit("addMessage", msg);
        }
      }

      //点击某个用户 或者 群发，分成 私聊和公聊
      $scope.setReceiver = function (receiver) {
        $scope.receiver = receiver;
        if (receiver) { //私信用户
          if (!$scope.privateMessages[receiver]) {
            $scope.privateMessages[receiver] = [];
          }
          $scope.messages = $scope.privateMessages[receiver];
        } else {//广播
          $scope.messages = $scope.publicMessages;
        }
        var user = userService.get($scope.users, receiver);
        if (user) {
          user.hasNewMessage = false;
        }
      }

      //收到登录结果---我自己的登陆结果，即emit("addUser" 到服务器之后的返回结果
      socket.on('userAddingResult', function (data) {
        if (data.result) {
          $scope.userExisted = false;
          $scope.hasLogined = true;
        } else {//昵称被占用
          $scope.userExisted = true;
        }
      });

      //接收到欢迎新用户消息---- 这是其他人的登陆结果，追加到本地的 user 列表
      socket.on('userAdded', function (data) {
        if (!$scope.hasLogined) return;
        $scope.publicMessages.push({text: data.nickname, type: "welcome"});
        $scope.users.push(data);
      });

      //接收到在线用户消息 ---- 这是首次登陆后，直接接收已经在线的user列表
      socket.on('allUser', function (data) {
        if (!$scope.hasLogined) return;
        $scope.users = data;
      });

      //接收到用户退出消息 ---- 这是其他人的离开结果，本地的 user 列表中删除掉该 user
      socket.on('userRemoved', function (data) {
        if (!$scope.hasLogined) return;
        $scope.publicMessages.push({text: data.nickname, type: "bye"});
        for (var i = 0; i < $scope.users.length; i++) {
          if ($scope.users[i].nickname == data.nickname) {
            $scope.users.splice(i, 1);
            return;
          }
        }
      });

      //接收到新消息
      socket.on('messageAdded', function (data) {
        if (!$scope.hasLogined) return;
        if (data.to) { //私信
          if (!$scope.privateMessages[data.from]) {
            $scope.privateMessages[data.from] = [];
          }
          $scope.privateMessages[data.from].push(data);
        } else {//群发
          $scope.publicMessages.push(data);
        }
        var fromUser = userService.get($scope.users, data.from);
        var toUser = userService.get($scope.users, data.to);
        if ($scope.receiver !== data.to) {//与来信方不是正在聊天当中才提示新消息
          if (fromUser && toUser.nickname) {
            fromUser.hasNewMessage = true;//私信
          } else {
            toUser.hasNewMessage = true;//群发
          }
        }
      });


    }]);
