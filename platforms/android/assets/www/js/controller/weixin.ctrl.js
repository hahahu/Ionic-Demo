angular.module('starter.weixin.ctrl', [])

  .controller('WeixinCtrl', function ($scope) {
      console.log("WeixinCtrl >>>>>>");

      var fn = function (msg) {
        var deffer = $q.defer();
        deffer.resolve(msg);
        return deffer.promise;
      };

      $scope.scanQRCode = function () {
        wx.scanQRCode({
          desc: 'scanQRCode desc',
          needResult: 1, // 默认为0，扫描结果由微信处理，1则直接返回扫描结果，
          scanType: ["qrCode", "barCode"], // 可以指定扫二维码还是一维码，默认二者都有
          success: function (res) {
            fn(res).then(function (res) {//此代码保证可以读写$scope
              console.log("scanQRCode res === "+ res);
              $scope.addEntity.product.code = res.resultStr.split(",")[1];
            });
          }
        });
      };


      $scope.shareToFriends = function () {
        console.log("WeixinCtrl 000 >>>>>>");
        wxData = {
          "appId": "",
          "imgUrl": "http://shuixiehui.dhbm.cn/images/icon.png",
          "link": "192.168.1.101/index.html",
          "desc": "世界那么大，我想去看看",
          "title": "Ionic Demo"
        };

        WeixinApi.ready(function (Api) {
          // 分享的回调
          console.log("分享的回调 WeixinApi ");
          var wxCallbacks = {
            // 分享操作开始之前
            ready: function () {
              // 你可以在这里对分享的数据进行重组
              console.log("你可以在这里对分享的数据进行重组 ");
            },
            // 分享被用户自动取消
            cancel: function (resp) {
              // 你可以在你的页面上给用户一个小Tip，为什么要取消呢？
              console.log("你可以在你的页面上给用户一个小Tip，为什么要取消呢？");
            },
            // 分享失败了
            fail: function (resp) {
              // 分享失败了，是不是可以告诉用户：不要紧，可能是网络问题，一会儿再试试？
              console.log("分享失败了，是不是可以告诉用户：不要紧，可能是网络问题，一会儿再试试？");
            },
            // 分享成功
            confirm: function (resp) {
              // 分享成功了，我们是不是可以做一些分享统计呢？
              console.log("分享成功了，我们是不是可以做一些分享统计呢？");
            },
            // 整个分享过程结束
            all: function (resp) {
              // 如果你做的是一个鼓励用户进行分享的产品，在这里是不是可以给用户一些反馈了？
              console.log("如果你做的是一个鼓励用户进行分享的产品，在这里是不是可以给用户一些反馈了");
            }
          };

          //WeixinJSBridge.invoke("sendAppMessage",wxData, wxCallbacks);


          // 用户点开右上角popup菜单后，点击分享给好友，会执行下面这个代码
          Api.shareToFriend(wxData, wxCallbacks);
          //Api.invoke('sendAppMessage',wxData,wxCallbacks);
          //Api.sendAppMessage(wxData);
        });


      }
    }
  );
