angular.module('starter.playlists.ctrl', [])
  //传入 NewPlayers
  .controller('PlaylistsCtrl', function ($scope, Globals, NewPlayers, $ionicPlatform, $state) {
    $scope.noMoreData = false;//标识是否还有新数据，控制不再刷新
    //$scope.playlists = [];//初始化空
    $scope.isPulling = false;

    $scope.playlists = NewPlayers.getFeedPlayers().then(function (newItems) {
      console.debug("NewPlayers >>>>> getFeedPlayers");
      if (newItems == null) {
        Globals.mPlayLists = null;
        console.debug("空空---------------!");
        return;
      }
      //by wzh 20160418 api变了,没有user,username在login了
      console.log(newItems[0].login.username);
      //console.log(newItems[0].picture.medium);
      //console.log(newItems[0].location.city);
      $scope.playlists = newItems;

      Globals.mPlayLists = $scope.playlists;

      //奇怪,还有串门,第一次先假设也在 pull
      $scope.isPulling = true;
    });


    //add by wzh 20160728
    $scope.onPulling = function () {
      console.log("上拉刷新数据啦！onPulling>>>>>>");
      $scope.isPulling = true;
    };

    //add by wzh 20160109 下来刷新处理
    $scope.doRefresh = function () {
      console.log("上拉刷新数据啦！222555>>>>>>");
      $scope.isPulling = false;

      if ($scope.noMoreData == true) {
        //by wzh 20160728 没有数据了,不要发送广播 $scope.$broadcast('scroll.refreshComplete');
        return;
      }

      NewPlayers.getNewPlayers().then(function (newItems) {
        if (newItems == null) $scope.noMoreData = true;
        else {
          //往前插 $scope.playlists = newItems.concat($scope.playlists);
          //往后追 $scope.playlists = $scope.playlists.concat(newItems);

          //注意 concat ：谁在前，谁在后？谁追加到谁？
          $scope.playlists = newItems.concat($scope.playlists);
          Globals.mPlayLists = $scope.playlists;
        }

        //怎么结束呢？发送一个广播 Stop the ion-refresher from spinning
        $scope.$broadcast('scroll.refreshComplete');
      });

    };



//add by wzh 20160115 滚到头 加载更多，这次往后追加数据
    $scope.loadMoreData = function () {
      console.log("加载更多 刷新数据啦！333444>>>>>>");
      if ($scope.noMoreData == true) {
        //by wzh 20160728 没有数据了,不要发送广播 $scope.$broadcast('scroll.infiniteScrollComplete');
        return;
      }

      NewPlayers.getNewPlayers().then(function (newItems) {
        if (newItems == null) $scope.noMoreData = true;

        //往前插 $scope.playlists = newItems.concat($scope.playlists);
        //往后追 $scope.playlists = $scope.playlists.concat(newItems);

        //往前插 $scope.playlists = newItems.concat($scope.playlists);
        var x = $scope.playlists;
        //var bbb = newItems.concat(x);
        $scope.playlists = newItems.concat(x);

        Globals.mPlayLists = $scope.playlists;

        //怎么结束呢？发送一个广播 Stop the ion-refresher from spinning
        $scope.$broadcast('scroll.infiniteScrollComplete');
      });


    };


    //所有数据都加载了，没了！挡不住啊！待处理
    $scope.moreDataCanBeLoaded = function () {
      if ($scope.noMoreData) {
        console.log("noMoreData 111  === " );
        return true;
      }
      //add by wzh 20160728 上拉的时候,不要下拉刷新
      if ($scope.isPulling) {
        console.log("isPulling 222 === " );
        return true;
      }

      //add by wzh 20160728 第一次数据加载完(getFeed)之前,也是没有数据的
      //超过嘴大,也算没有
      var x = $scope.playlists.length;
      var FeedCount = 1;
      var MaxCount = 200;//按照实际情况自行定义
      if (x < FeedCount || x > MaxCount) {
        console.log("已经这么多数据了 === " + x);
        $scope.noMoreData = true;
        return true;
      }
    };

    //2060129 getThisPlayer123从 appCtrl 转移到这里，他属于这里
    $scope.getThisPlayer123 = function (playlist) {
      $state.go("single.show", {
        "playlistId": playlist.login.salt
      });
    };

  });

