angular.module('starter.device.ctrl', [])
  //add by wzh 20160212
  .controller('DeviceCtrl', function ($scope, $state, $cordovaDevice, MsgBox) {

    var sss = "这个功能只能在手机使用";
    if (!window.cordova) {
      MsgBox.show(sss);
      return;
    }

    var init = function () {
      console.log("initializing device");
      try {
        document.addEventListener("deviceready", function () {
          $scope.available = $cordovaDevice.getDevice().available;
          //$scope.available = $cordovaDevice//.getName //.getManufacturer()

          $scope.cordova = $cordovaDevice.getCordova();
          $scope.model = $cordovaDevice.getModel();
          $scope.platform = $cordovaDevice.getPlatform();
          $scope.uuid = $cordovaDevice.getUUID();
          $scope.version = $cordovaDevice.getVersion();
        }, false);
      }
      catch (err) {
        console.log("Error " + err.message);
        alert("error " + err.$$failure.message);
      }
    };

    init();

  });

