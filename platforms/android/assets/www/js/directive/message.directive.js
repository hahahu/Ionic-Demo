angular.module('starter.directives.message', [])
  .directive('message', ['$timeout', function ($timeout) {
    return {
      restrict: 'E',
      templateUrl: './templates/message.html',
      //为指令生成了隔离作用域
      //@，它将本地作用域和DOM中的属性值绑定起来
      //=与@的不同点在于，@是针对字符串而用，但=是针对某个对象的引用，
      //&符号含义是：对父级作用域进行绑定，并将其中的属性包装成一个函数
      scope: {
        info: "=",
        self: "=",
        scrolltothis: "&"
      },
      //如果要开放出一个API给其他指令用就写在controller中，否则写在link中
      //可以简单理解为，当directive被angular 编译后，执行该方法
      //link函数主要用来操作DOM的
      //link中第一个参数scope基本上就是那个scope
      //elem 就是$('XXXX'),关联的那个DOM元素
      //attrs 就是关联的那个DOM元素的属性，例如{ type: 'modal',animation: 'fade',self="nickname" ...

      link: function (scope, elem, attrs) {
        scope.time = new Date();
        $timeout(scope.scrolltothis);
        $timeout(function () {
          elem.find('.avatar').css('background', scope.info.color);
        });
      }
    };
  }])
