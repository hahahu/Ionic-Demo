//注册我自己定义的指令 ： 指令注册的方式与 controller 一样,它返回的是一个拥有指令配置属性的简单对象
//第一个参数是这个指令的名字
//第二个参数是一个返回指令定义对象的函数
//如果你的指令依赖于其他的对象或者服务，比如 $rootScope, $http, 或者$compile，他们可以在这个时间被注入
//restrict:这个属性用来指定指令在HTML中如何使用
//template: 这个属性规定了指令被Angular编译和链接（link）后生成的HTML标记
//replace: 这个属性指明生成的HTML内容是否会替换掉定义此指令的HTML元素
//四种形式：E:元素 A:属性 C:CLASS M:注释

angular.module('starter.directives', [])


//add by wzh 20160219 ng 没有 ng-onload事件,自定义一个
//ng 2.0 去掉了jqLite?那就只能这么做了？没有替代事件吗？
//参考 http://stackoverflow.com/questions/17547917/angularjs-image-onload-event/17548090
//怎么不管用?
//参考 http://stackoverflow.com/questions/20863891/angularjs-directive-put-a-call-function-in-an-attribute-without-including-anot
//还是不太明白,留下这2个,下次再来学习

  .directive('imgLoad', function() { // 'imgLoad'
  return {
    restrict: 'A',
    scope: {
      loadHandler: '&imgLoad' // 'imgLoad'
    },
    link: function (scope, element, attr) {
      console.log("attr ==="+attr);
      element.on('load', scope.loadHandler);
    }
  };
})


;
