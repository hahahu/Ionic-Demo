angular.module('starter.directives.user', [])
.directive('user', ['$timeout',function($timeout) {
  return {
    restrict: 'E',
    templateUrl: './templates/user.html',
    scope:{
      info:"=",
      iscurrentreceiver:"=",
      setreceiver:"&"
    },
    link:function(scope, elem, attrs,ChatCtrl){
      $timeout(function(){
        elem.find('.avatar').css('background',scope.info.color);
      });
    }
  };
}]);
