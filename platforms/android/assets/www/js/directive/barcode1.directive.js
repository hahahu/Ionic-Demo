angular.module('starter.directives.barcode1', [])
.directive('barcode1', function () {
  //var urlDHBM = "www.dhbm.cn";
  console.log("barcode1 === barcode1 ======== ");
  return {
    restrict: "AE",
    scope: {
      text: '='
    },
    link: function (scope, elem, attrs) {
      var $elem = $(elem);
      //这是获取该元素，以下的操作就是针对他

      var text = scope.text; //attrs.text;

      //判断实用的标签:作为 A 还是作为 E ？tagName返回结果全部大写
      //保证他无论作为 A 还是 E ，都能够获取到正确的尺寸
      var x = $elem.get(0).tagName;
      //console.log("$elem.type 111 === " + x);
      if (x == angular.uppercase('barcode1')) {
        //console.log("$elem.type 222 === " + x);
        var width = attrs.width;
        var height = attrs.height;
        var img = attrs.img;
      }
      else {
        width = $elem.width();
        height = $elem.height();
        img = attrs.img;
      }


      //console.log("$elem.tagname === " + $elem.prototype );

      //console.log("text111=== " + text);
      //console.log("width111=== " + width);
      //console.log("height111=== " + height);

      //这是 jq 生成二维码的插件的方法
      $elem.qrcode({
        render: 'canvas', //"table", //table方式
        width: width,
        height: height,
        text: text,
        src: img//'img/body.jpq'
      });

    }
  };

});
