angular.module('starter.services.checkLogin', [])
  //登录验证功能，待转移到这里
  .factory('CheckLogin', function ($http,NewFriends) {
    //var WAIT_TIMEOUT = 10000;
    var HTTP_TIMEOUT = 20000;
    //var loginData = {};
// 这个模拟超时 var USER_API_URL = "http://127.0.0.111/angularjs/data/Login_JSON.php";
    // 唱歌模拟登录错误 var USER_API_URL = "http://127.0.0.1/angularjs/data/Err_Login_JSON.php";
    //var USER_API_URL = "http://127.0.0.1/angularjs/data/Login_JSON.php";

    // 这个模拟超时 var USER_API_URL = "data111/Login_JSON.php";
    // 唱歌模拟登录错误
    //var USER_API_URL = "data/Err_Login_JSON.php";
    var USER_API_URL = "data/Login_JSON.php";

    return {
      validate: function (loginData) {
        if (loginData.username == null) return false;
        if (loginData.password == null) return false;

        console.log('loginData.username.length =============', loginData.username.length);
        //简单验证输入部分，完善的规则待处理
        var x = loginData.username.length;
        var y = loginData.password.length;
        return !(x < 3 || y < 6)
      },

      checking: function (loginData) {

        return $http.get(USER_API_URL, {timeout: HTTP_TIMEOUT}).then(function (response) {
            //console.log('response ====', response + "time== " + new Date().toLocaleString());
            var errcode = response.data.errcode;

            //by wzh 20160418 没有实际的php逻辑处理,在这里伪造一个本地判断,
            //存在于 friends 的为 errcode = 0 ,否则 errcode = 40023
            //根据 response 即结果： sucess/fail，分别处理
            var userName = loginData.username;
            if (NewFriends.getByName(userName))
            {
              errcode = "0";
              console.log('该用户存在 ,欢迎您====');
            }
            else  {
              errcode = "40023";
              console.log('该用户不存在 ,重新试试====');
            }

            if (errcode > "0") {
              //登陆错误 log 错误原因备查
              var errmsg = response.data.errmsg;
              console.log('errmsg =============', errmsg);
              return null;
            }
            else {
              //追加保存到 loginData,同时设置为：已登录！
              var x = {"hasLogined": "true"};
              angular.extend(x, loginData, response.data);
              loginData = x;
              //console.log('$scope.x =============', x);
              return loginData;
            }
          }
        );
      }
    }

  });

