angular.module('starter.services.msgBox', [])
.factory('MsgBox', function ($ionicPopup) {
  return {
    show: function (msg, title123) {
      if (msg == null) return;
      var sss = msg;//"正在开发中111111......";
      if (title123 == null || title123 == 'undefined')
        title123 = "友情提示";

      var alertPopup = $ionicPopup.alert(
        {
          title: title123,
          template: sss
        }
      );
      alertPopup.then(function (res) {
        console.log("res >>>>>>>>>>>>>>" + res);
      });
    }
  }
});
