angular.module('starter.services.toast', [])
//add by wzh 20160307 原生提示,只做简单提示
// by wzh 20160308 $cordovaToast.showLongCenter(message)不带后续参数也可以使用啊!!
// 待删除
.factory("Toast", function ($cordovaToast){
  return {
    showLongCenter: function (message) {
      $cordovaToast.showLongCenter(message)
        .then(function (success) {
          console.log("center msg displayed");
        }, function (error) {
          $scope.msg = error.message;
        });
    },
    showLongTop: function (message) {
      $cordovaToast.showLongTop(message)
        .then(function (success) {
          console.log("top msg displayed");
        }, function (error) {
          $scope.msg = error.message;
        });
    },
    showLongBottom: function (message) {
      $cordovaToast.showLongBottom(message)
        .then(function (success) {
          console.log("bottom msg displayed");
        }, function (error) {
          $scope.msg = error.message;
        });
    },

    showShortCenter: function (message) {
      $cordovaToast.showShortCenter(message)
        .then(function (success) {
          console.log("center msg displayed");
        }, function (error) {
          $scope.msg = error.message;
        });
    },
    showShortTop: function (message) {
      $cordovaToast.showShortTop(message)
        .then(function (success) {
          console.log("top msg displayed");
        }, function (error) {
          $scope.msg = error.message;
        });
    },
    showShortBottom: function (message) {
      $cordovaToast.showShortBottom(message)
        .then(function (success) {
          console.log("bottom msg displayed");
        }, function (error) {
          $scope.msg = error.message;
        });
    }

  }


});
