angular.module('starter.services.checkNavigator', [])
.factory('CheckNavigator', function () {
  return {
    versions: function () {
      var u = navigator.userAgent;
      var app = navigator.appVersion;
      console.log("app=" + app);
      //alert("appVersion 版本信息= \n" + app);
      //alert("userAgent 注册信息= \n" + u);
      var ret = {
        trident: u.indexOf('Trident') > -1, //IE内核
        presto: u.indexOf('Presto') > -1, //opera内核
        webKit: u.indexOf('AppleWebKit') > -1, //苹果、谷歌内核
        gecko: u.indexOf('Gecko') > -1 && u.indexOf('KHTML') == -1, //火狐内核
        mobile: !!u.match(/AppleWebKit.*Mobile.*/) || !!u.match(/AppleWebKit/), //是否为移动终端
        ios: !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/), //ios终端
        android: u.indexOf('Android') > -1 || u.indexOf('Linux') > -1, //android终端或者uc浏览器
        iPhone: u.indexOf('iPhone') > -1 || u.indexOf('Mac') > -1, //是否为iPhone或者QQHD浏览器
        iPad: u.indexOf('iPad') > -1, //是否iPad
        webApp: u.indexOf('Safari') == -1, //是否web应该程序，没有头部与底部
        wx: u.indexOf('MicroMessenge') != -1//add by wzh 20140716 是否WX
      };

      console.log("ret=" + ret.trident);
      return ret;

    },

    log: function () {
      var browser = this.versions();
      var sss = " 是否为移动终端: " + browser.mobile;
      sss = sss + "<br>";
      sss = sss + " ios终端: " + browser.ios;
      sss = sss + "<br>";
      sss = sss + " android终端: " + browser.android;
      sss = sss + "<br>";
      sss = sss + " 是否为iPhone: " + browser.iPhone;
      sss = sss + "<br>";
      sss = sss + " 是否iPad: " + browser.iPad;
      sss = sss + "<br>";

      //add by wzh 20140725
      sss = sss + "---------------------";
      sss = sss + "<br>";
      sss = sss + " 是否微信: " + browser.wx;
      sss = sss + "<br>";
      //add by wzh 20140725
      sss = sss + " 是否内置webKit: " + browser.webKit;
      sss = sss + "<br>";
      sss = sss + " 是否火狐: " + browser.gecko;
      sss = sss + "<br>";
      //sss = sss + " 是否Opera: " + browser.presto;
      //sss = sss + "<br>";
      sss = sss + " 是否IE: " + browser.trident;
      sss = sss + "<br>";
      sss = sss + " 是否google/android: " + browser.android;
      sss = sss + "<br>";
      sss = sss + " 是否苹果safari " + browser.webKit;
      sss = sss + "<br>";

      //add by wzh 20140725
      sss = sss + "---------------------";
      sss = sss + "<br>";

      //add by wzh 20140710
      if (!(document.cookie || navigator.cookieEnabled)) {
        sss = sss + 'cookie 未打开!';
        sss = sss + "<br>";
      } else {
        sss = sss + 'cookie 打开啦！!';
        sss = sss + "<br>";
      }


      if (window.localStorage) {
        sss = sss + '浏览器支持本地存储!';
        sss = sss + "<br>";
      } else {
        sss = sss + '浏览器不支持本地存储，建议升级！';
        sss = sss + "<br>";
      }

      if (window.cordova) {
        sss = sss + 'Cordova插件已经加载!';
        sss = sss + "<br>";
      } else {
        sss = sss + '没有加载Cordova插件';
        sss = sss + "<br>";
      }


      return sss;
    }
  };
});
