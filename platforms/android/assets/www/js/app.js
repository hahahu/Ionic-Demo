// Ionic Starter App


// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.routes', 'ngCordova',
    'starter.controllers', 'starter.services', 'starter.directives',
    //ctrl
    'starter.file.ctrl', 'starter.fileOpener2.ctrl',
    'starter.fileTransfer.ctrl', 'starter.localNotification.ctrl',
    'starter.weixin.ctrl', 'starter.toastTest.ctrl',
    'starter.playlists.ctrl', 'starter.playlist.ctrl',
    'starter.slider.ctrl', 'starter.setting.ctrl',
    'starter.menuSetting.ctrl', 'starter.barcode.ctrl',
    'starter.network.ctrl', 'starter.imagePicker.ctrl',
    'starter.device.ctrl', 'starter.single.ctrl',
    'demo.contacts.ctrl', 'starter.chat.ctrl',
    'starter.chatDetail.ctrl', 'starter.chatRoot.ctrl',
    'starter.checkUpdate.ctrl',
    //service
    'starter.services.toast', 'starter.services.msgBox',
    'starter.services.localStorage', 'starter.services.checkLogin',
    'starter.services.checkNavigator', 'starter.services.getImgSize',
    'starter.services.newPlayers', 'starter.services.socket',
    'starter.services.userService', 'starter.services.randomColor',
    'starter.services.messages', 'starter.services.getApkVersion',
    //directive
    'starter.directives.sbLoad', 'starter.directives.barcode1',
    'starter.directives.user', 'starter.directives.message',
    'starter.directives.chat'
  ])

  .run(function ($ionicPlatform, $state,
                 $rootScope, $ionicHistory,
                 $ionicModal, $cordovaKeyboard,
                 $cordovaNetwork, $cordovaToast,
                 Globals, localStorageService,
                 MsgBox, GetApkVersion,
                 NewFriends, OriginChatMessages) {
    $ionicPlatform.ready(function () {
      //by wzh 20160308 这个需要 cordova 支持,所以不用判断设备了,只有app才有用
      if (window.cordova) {
        /*
         var newtork = $cordovaNetwork.getNetwork();
         console.log("newtork ====" + newtork);

         if (newtork !== 'wifi' && newtork !== 'Connection.WIFI') {
         var sss = '检查到当前使用的网络不是 Wi-Fi，将会产生网络流量';
         MsgBox.show(sss);
         }
         //测试用
         else {
         var sss = '检查到当前使用的网络是 Wi-Fi，不会产生网络流量(测试版本才有)';
         MsgBox.show(sss);
         }
         */
      }

      //20160418 伪造聊天数据
      /* by wzh 20160720 先去掉,待转移
       NewFriends.getAll();
       OriginChatMessages.get();
       */

      var x = localStorageService.get("userInfo");
      if (x != null) Globals.mLoginData = x;
      //console.log('Globals.mLoginData 1111  =============', Globals.mLoginData);

      if (window.cordova && window.cordova.plugins.Keyboard) {
        //console.log('window.cordova.plugins.Keyboard 有啦!============');

        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        cordova.plugins.Keyboard.disableScroll(true);
      }
      else {
        console.log('window.cordova.plugins.Keyboard 没有啊>>>>>>>');
      }

      if (window.StatusBar) {
        // org.apache.cordova.statusbar required
        StatusBar.styleDefault();
      }

      //首次直接使用浏览器判断在线与否
      if (navigator.onLine) {
        Globals.mLoginData.offline = false;//
        //自动升级处理
        var apkUrl = "http://api2.dhbm.cn/wap/GetVersionAppH5/?key=3414d916c27e14bc3e9e08a30660d5ec268e76d009aee2a2c5a828973f40d223&appid=8880018&flag=com.dhbm.lmsd&encoding=utf-8&resulttype=json";

        GetApkVersion.upgradeApp(apkUrl);
      } else {
        Globals.mLoginData.offline = true;//false;//
      }


      //add by wzh 20160307 微信api配置
      /* 待处理
       wx.config({
       debug: true, // 开启调试模式,调用的所有api的返回值会在客户端alert出来，若要查看传入的参数，可以在pc端打开，参数信息会通过log打出，仅在pc端时才会打印。
       appId: 'wx84c58aa8c921ba93', // 必填，公众号的唯一标识
       timestamp: , // 必填，生成签名的时间戳
       nonceStr: '', // 必填，生成签名的随机串
       signature: '',// 必填，签名，见附录1
       jsApiList: [] // 必填，需要使用的JS接口列表，所有JS接口列表见附录2
       });

       */

      //add by wzh 20160316 监听键盘显示事件,后续如果需要进一步控制的话,先保留
      window.addEventListener('native.keyboardshow', function () {
        console.log('keyboardDidShow >>>>>>');
        $rootScope.isVisibleKeyboard = true;
      });

      window.addEventListener('native.keyboardhide', function () {
        // Describe your logic which will be run each time keyboard is closed.
        console.log('keyboardDidHide <<<<<<<<<<');
        setTimeout(function () {
          $rootScope.isVisibleKeyboard = false;
        }, 1000);

      });

      //add by wzh 20160308 双击退出
      $ionicPlatform.registerBackButtonAction(function (e) {
        //判断处于哪个页面时双击退出
        //console.log('$state.$current.name  =============' + $state.$current.name);
        //console.log('$state.$current.url  =============' + $state.$current.url);

        //这个好像不准确,先保留,改由监控处理
        var isVisible = $cordovaKeyboard.isVisible();
        console.log('回退键盘按下啦!键盘状态111 =====' + isVisible);
        isVisible = $rootScope.isVisibleKeyboard;
        console.log('回退键盘按下啦!键盘状态222 =====' + isVisible);

        if ($state.includes('app.playlists')) {
          if ($rootScope.backButtonPressedOnceToExit) {
            ionic.Platform.exitApp();
          } else {
            $rootScope.backButtonPressedOnceToExit = true;
            $cordovaToast.showShortCenter('再按一次退出系统!!!');
            setTimeout(function () {
              $rootScope.backButtonPressedOnceToExit = false;
            }, 2000);
          }
        }
        else if ($ionicHistory.backView()) {
          if (isVisible) {
            $cordovaKeyboard.close();
            //$rootScope.isVisibleKeyboard = false;
          }
          else {
            $ionicHistory.goBack();
          }
        } else {
          //一般不会走到这里,除非clear历史,或改变过home
          $rootScope.backButtonPressedOnceToExit = true;
          $cordovaToast.showShortCenter('再按一次退出系统!');
          setTimeout(function () {
            $rootScope.backButtonPressedOnceToExit = false;
          }, 2000);
        }
        e.preventDefault();
        return false;
      }, 401);
      //101 : IONIC_BACK_PRIORITY 如果有优先级更高的事件,回退就会无效(view 的优先级 =100)
      //优先级 401
    });
  });

