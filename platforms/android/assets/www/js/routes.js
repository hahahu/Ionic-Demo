angular.module('starter.routes', ['ionic', 'starter.controllers', 'starter.services'])

//这里就是本app的所有的 router
.config(function ($stateProvider, $urlRouterProvider) {
  $stateProvider

    .state('app', {
      url: '/app',
      abstract: true,
      templateUrl: 'templates/menu.html',
      controller: 'AppCtrl'
    })

    .state('app.sliders', {
      url: '/sliders',
      views: {
        'menuContent': {
          templateUrl: 'templates/sliders.html',
          controller: 'SlidersCtrl'
        }
      }
    })

    .state('app.contacts', {
      url: '/contacts',
      views: {
        'menuContent': {
          templateUrl: 'templates/contacts.html',
          controller: 'ContactsCtrl'
        }
      }
    })

    .state('app.playlists', {
      url: '/playlists',
      views: {
        'menuContent': {
          templateUrl: 'templates/playlists.html',
          controller: 'PlaylistsCtrl'
        }
      }
    })

    .state('app.setting', {
      url: '/setting',
      views: {
        'menuContent': {
          templateUrl: 'templates/setting.html',
          controller: 'SettingCtrl'
        }
      }
    })

    .state('app.menuSetting', {
      url: '/menuSetting',
      views: {
        'menuContent': {
          templateUrl: 'templates/menuSetting.html',
          controller: 'MenuSettingCtrl'
        }
      }
    })


    .state('app.barcode', {
      url: '/barcode',
      views: {
        'menuContent': {
          templateUrl: 'templates/qrcode.html',
          controller: 'BarcodeCtrl'
        }
      }
    })

    .state('single.device', {
      url: '/device',
      views: {
        'menuContent': {
          templateUrl: 'templates/device.html',
          controller: 'DeviceCtrl'
        }
      }
    })

    .state('app.network', {
      url: '/network',
      views: {
        'menuContent': {
          templateUrl: 'templates/network.html',
          controller: 'NetworkCtrl'
        }
      }
    })

    .state('app.file', {
      url: '/file',
      views: {
        'menuContent': {
          templateUrl: 'templates/file.html',
          controller: 'FileCtrl'
        }
      }
    })


    .state('chatRoot', {
      url: '/chatRoot',
      abstract: true,
      templateUrl: 'templates/chatRoot.html',
      controller: 'ChatRootCtrl'
    })

    .state('chatRoot.message', {
      url: '/message',
      views: {
        'menuContent': {
          templateUrl: 'templates/tab-message.html',
          controller: 'ChatCtrl'
        }
      }
    })

    .state('chatDetail', {
      url: '/chatDetail/:messageId',
      templateUrl: "templates/chatDetail.html",
      controller: "ChatDetailCtrl"
    })


    .state('app.fileOpener2', {
      url: '/fileOpener2',
      views: {
        'menuContent': {
          templateUrl: 'templates/fileOpener2.html',
          controller: 'FileOpener2Ctrl'
        }
      }
    })
    .state('app.fileTransfer', {
      url: '/fileTransfer',
      views: {
        'menuContent': {
          templateUrl: 'templates/fileTransfer.html',
          controller: 'FileTransferCtrl'
        }
      }
    })
    .state('app.localNotification', {
      url: '/localNotification',
      views: {
        'menuContent': {
          templateUrl: 'templates/localNotification.html',
          controller: 'LocalNotificationCtrl'
        }
      }
    })

    .state('single.imagePicker', {
      url: '/imagePicker',
      views: {
        'menuContent': {
          templateUrl: 'templates/imagePicker.html',
          controller: 'ImagePicker'
        }
      }
    })

    .state('app.toastTest', {
      url: '/toastTest',
      views: {
        'menuContent': {
          templateUrl: 'templates/toastTest.html',
          controller: 'ToastTestCtrl'
        }
      }
    })

  //add by wzh 20150720
    .state('app.checkUpdate', {
      url: '/checkUpdate',
      views: {
        'menuContent': {
          controller: 'checkUpdateCtrl'
        }
      }
    })

    .state('app.login', {
      url: '/login',
      views: {
        'menuContent': {
         // templateUrl: 'templates/login.html',
          controller: 'LoginCtrl'
        }
      }
    })

    .state('app.weixin', {
      url: '/weixin',
      views: {
        'menuContent': {
          templateUrl: 'templates/weixin.html',
          controller: 'WeixinCtrl'
        }
      }
    })

    //这是点击 playlists 的某一个 player 的 url
    .state('single', {
      url: '/single',
      templateUrl: 'templates/single.html',
      abstract : true,
      controller: 'SingleCtrl'
    })

  //这是点击 playlists 的某一个 player 的 url
  .state('single.show', {
    url: '/playlists/:playlistId',
    views: {
      'menuContent': {
        templateUrl: 'templates/playlist.html',
        controller: 'PlaylistCtrl'
      }
    }
  });

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/playlists');
});

