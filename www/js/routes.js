angular.module('starter.routes', ['ionic', 'starter.controllers', 'starter.services'])

  //这里就是本app的所有的 router
  .config(function ($stateProvider, $urlRouterProvider, $ionicConfigProvider) {
      $stateProvider

        .state('app', {
          url: '/app',
          abstract: true,
          templateUrl: 'templates/menu.html',
          controller: 'AppCtrl'
        })

        .state('app.contacts', {
          url: '/contacts',
          views: {
            'tab-menuSetting': {
              templateUrl: 'templates/contacts.html',
              controller: 'ContactsCtrl'
            }
          }
        })

        .state('app.playlists', {
          url: '/playlists',
          views: {
            'tab-playlists': {
              templateUrl: 'templates/playlists.html',
              controller: 'PlaylistsCtrl'
            }
          }
        })

        .state('app.setting', {
          url: '/setting',
          views: {
            'tab-setting': {
              templateUrl: 'templates/setting.html',
              controller: 'SettingCtrl'
            }
          }
        })

        .state('app.menuSetting', {
          url: '/menuSetting',
          views: {
            'tab-menuSetting': {
              templateUrl: 'templates/menuSetting.html',
              controller: 'MenuSettingCtrl'
            }
          }
        })


        .state('app.barcode', {
          url: '/barcode',
          views: {
            'tab-setting': {
              templateUrl: 'templates/qrcode.html',
              controller: 'BarcodeCtrl'
            }
          }
        })

        .state('single.device', {
          url: '/device',
          views: {
            'menuContent': {
              templateUrl: 'templates/device.html',
              controller: 'DeviceCtrl'
            }
          }
        })

        .state('app.network', {
          url: '/network',
          views: {
            'tab-menuSetting': {
              templateUrl: 'templates/network.html',
              controller: 'NetworkCtrl'
            }
          }
        })

        .state('app.file', {
          url: '/file',
          views: {
            'menuContent': {
              templateUrl: 'templates/file.html',
              controller: 'FileCtrl'
            }
          }
        })


        .state('chatRoot', {
          url: '/chatRoot',
          abstract: true,
          templateUrl: 'templates/chatRoot.html',
          controller: 'ChatRootCtrl'
        })

        .state('chatRoot.message', {
          url: '/message',
          views: {
            'menuContent': {
              templateUrl: 'templates/tab-message.html',
              controller: 'ChatCtrl'
            }
          }
        })

        .state('chatDetail', {
          url: '/chatDetail/:messageId',
          templateUrl: "templates/chatDetail.html",
          controller: "ChatDetailCtrl"
        })


        .state('app.fileOpener2', {
          url: '/fileOpener2',
          views: {
            'menuContent': {
              templateUrl: 'templates/fileOpener2.html',
              controller: 'FileOpener2Ctrl'
            }
          }
        })
        .state('app.fileTransfer', {
          url: '/fileTransfer',
          views: {
            'tab-menuSetting': {
              templateUrl: 'templates/fileTransfer.html',
              controller: 'FileTransferCtrl'
            }
          }
        })
        .state('app.localNotification', {
          url: '/localNotification',
          views: {
            'tab-menuSetting': {
              templateUrl: 'templates/localNotification.html',
              controller: 'LocalNotificationCtrl'
            }
          }
        })

        .state('single.imagePicker', {
          url: '/imagePicker',
          views: {
            'menuContent': {
              templateUrl: 'templates/imagePicker.html',
              controller: 'ImagePicker'
            }
          }
        })

        .state('app.toastTest', {
          url: '/toastTest',
          views: {
            'tab-menuSetting': {
              templateUrl: 'templates/toastTest.html',
              controller: 'ToastTestCtrl'
            }
          }
        })

        //add by wzh 20150720
        .state('app.checkUpdate', {
          url: '/checkUpdate',
          views: {
            'menuContent': {
              controller: 'checkUpdateCtrl'
            }
          }
        })

        .state('app.login', {
          url: '/login',
          views: {
            'menuContent': {
              // templateUrl: 'templates/login.html',
              controller: 'LoginCtrl'
            }
          }
        })

        .state('app.weixin', {
          url: '/weixin',
          views: {
            'menuContent': {
              templateUrl: 'templates/weixin.html',
              controller: 'WeixinCtrl'
            }
          }
        })

        //这是点击 playlists 的某一个 player 的 url
        .state('single', {
          url: '/single',
          templateUrl: 'templates/single.html',
          abstract: true,
          controller: 'SingleCtrl'
        })

        //这是点击 playlists 的某一个 player 的 url
        .state('single.show', {
          url: '/playlists/:playlistId',
          views: {
            'menuContent': {
              templateUrl: 'templates/playlist.html',
              controller: 'PlaylistCtrl'
            }
          }
        });

      //缺省路由到首页
      $urlRouterProvider.otherwise('/app/playlists');

      //设置 tabs 到底部
      $ionicConfigProvider.platform.ios.tabs.style('standard');
      $ionicConfigProvider.platform.ios.tabs.position('bottom');
      $ionicConfigProvider.platform.android.tabs.style('standard');
      $ionicConfigProvider.platform.android.tabs.position('bottom');

      $ionicConfigProvider.platform.ios.navBar.alignTitle('center');
      $ionicConfigProvider.platform.android.navBar.alignTitle('center');

    }
  );

