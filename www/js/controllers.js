angular.module('starter.controllers', [])

  .controller('AppCtrl', function ($scope, $state, $rootScope, $ionicModal,
                                   $ionicHistory, $window, localStorageService, $http,
                                   $ionicLoading, $ionicPopup, Globals, CheckLogin,
                                   $cordovaCamera, $cordovaBarcodeScanner
    , MsgBox, $cordovaNetwork) {

    //全局变量 mLoginData 会在app run的时候初始化
    $scope.loginData = Globals.mLoginData;

    //by wzh 20160227 在这里监控
    $rootScope.$on("$cordovaNetwork:offline", function (event, result) {
      //alert("您的网络断开了!请检查...");
      console.log('event=====' + event);
      console.log('result=====' + result);
      Globals.mLoginData.offline = true;
      $scope.loginData.offline = true;
    });

    $rootScope.$on("$cordovaNetwork:online", function (event, result) {
      //alert("您的网络恢复正常了!恭喜您...");
      console.log('event=====' + event);
      console.log('result=====' + result);
      Globals.mLoginData.offline = false;
      $scope.loginData.offline = false;
    });


    var BROADCAST_NAME = 'localstorage.clear';
    $scope.$on(BROADCAST_NAME, function (event, data) {
      //现在登录只有一个事件，以后增加 event 之后再扩展
      console.log('收到广播啦 =============' + data);
      //$scope.loginData.$delete();
      //Globals.mLoginData.$delete();
      $scope.loginData = {};
      Globals.mLoginData = {};
    });

    var templateUrl = 'templates/login.html';
    $ionicModal.fromTemplateUrl(templateUrl, {
      scope: $scope
    }).then(function (modal) {
      $scope.modal = modal;
    });

    // Triggered in the login modal to close it
    $scope.closeLogin = function () {
      // $scope.loginData.loginError = false;

      $scope.modal.hide();
      console.log("closeLogin！closeLogin >>>>>>");
    };


    $scope.registerNow = function () {
      // $scope.loginData.loginError = false;
      MsgBox.show("正在开发中 22222333......");
    };


    $scope.forgetPassword = function () {
      // $scope.loginData.loginError = false;
      MsgBox.show("正在开发中 22222333......", "找回密码提示");
    };

    $scope.showBigImg = function () {
      MsgBox.show("正在开发中 22222333......");
    };

    $scope.login = function () {
      $scope.loginData.loginError = false;
      $scope.modal.show();
    };

    $scope.doLogin = function () {
      //开始加载进度条。close前 隐藏
      $ionicLoading.show({
        template: '<ion-spinner icon="lines" class="spinner-calm"></ion-spinner>'
      });

      //先检查输入的合法性，再进行登录验证
      if (!CheckLogin.validate($scope.loginData)) {
        $scope.loginData.loginError = true;

        console.log("chenking！fail >>>>>>");
        //登录错误
        $ionicLoading.hide();
      }
      else {
        CheckLogin.checking($scope.loginData).then(function (loginData) {
            console.log("chenking！chenking x== >>>>>>" + loginData);
            if (loginData != null) {
              console.log("chenking！ok >>>>>>");
              $scope.loginData.loginError = false;
              $scope.loginData = loginData;
              //保存到全局，一边其他模块使用
              Globals.mLoginData = loginData;
              //保存到 LS，下次使用
              localStorageService.set("userInfo", loginData);

              $ionicLoading.hide();
              $scope.closeLogin();
            }
            else {
              $scope.loginData.loginError = true;

              console.log("chenking！fail >>>>>>");
              //登录错误
              $ionicLoading.hide();
              //$scope.closeLogin();
            }
          }
        );
      }
    };


    //add by wzh 20160130 扫描二维码
    $scope.scanBarcode = function () {
      if (!window.cordova) {
        MsgBox.show("这个功能只能在手机使用.....");
      }
      else if ($cordovaBarcodeScanner == null) {
        MsgBox.show("$cordovaBarcodeScanner : 这个插件没有安装或者引入");
      }
      else {
        $cordovaBarcodeScanner.scan().then(function (imageData) {
            //取消了，直接回去
            //if (imageData.cancelled) return;

            console.log("Barcode Format -> " + imageData.format);
            console.log("Cancelled -> " + imageData.cancelled);

            var sss = "imageData.text 内容<br>" + imageData.text;
            sss = sss + "<br>";
            sss = sss + "imageData.format 格式<br> " + imageData.format;
            sss = sss + "<br>";
            //sss = sss + " imageData.cancelled >  " + imageData.cancelled;
            //ss = sss + "<br>";

            var alertPopup = $ionicPopup.alert(
              {
                title: '二维码信息',
                template: sss
              }
            );
            alertPopup.then(function (res) {
              console.log("res >>>>>>>>>>>>>>" + res);
            });

          }, function (error) {
            console.log("An error happened -> " + error);
          }
        );
      }


    };

    //add by wzh 20160117 将 href 改成 ng-click
    $scope.goHome = function () {
      //在这里跳转
      $state.go("app.playlists");
    };

    $scope.slidersShow = function () {
      //在这里跳转
      $state.go("app.sliders");
    };


    $scope.login123 = function () {
      $state.go("app.login");
    };

    $scope.logout = function () {
      var sss1 = "您真的要退出吗？";
      console.log("sss1 >>>>>>>>>>>>>>" + sss1);
      var confirmPopup = $ionicPopup.confirm(
        {
          title: '友情提示',
          template: sss1
        }
      );
      confirmPopup.then(function (res) {
        if (res) {
          //取消他的登录状态，也清除他的记录，记住他？以后再说
          //$scope.loginData.$delete();
          //Globals.mLoginData.$delete();
          $scope.loginData = {};
          Globals.mLoginData = {};

          localStorageService.remove("userInfo");

          console.log(sss1);
        } else {
          console.log('您取消了！啥都没有做');
        }
      });
    };

    $scope.reconnectNetwork = function () {
      //直接检测网络,返回 online/offline
      $scope.networkType = $cordovaNetwork.getNetwork();

      if ($cordovaNetwork.isOnline()) {
        Globals.mLoginData.offline = false;
        $scope.loginData.offline = false;
      }
      else if ($cordovaNetwork.isOffline()) {
        Globals.mLoginData.offline = true;
        $scope.loginData.offline = true;
      }
    }
  });


