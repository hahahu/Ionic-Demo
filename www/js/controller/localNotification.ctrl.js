angular.module('starter.localNotification.ctrl', [])

  .controller('LocalNotificationCtrl', function ($scope, $rootScope, $cordovaLocalNotification, MsgBox) {
    var sss = "<b><h2> 这个功能只能在手机使用</h2></b>";
    if (!window.cordova) {
      MsgBox.show(sss);
      return;
    }

    $scope.addNotification = function (sound) {
      var now = new Date();
      var _2_seconds_from_now = new Date(now + 2 * 1000);
      var event = {
        //id: 1,
        at: _2_seconds_from_now,
        title: "测试消息Title",
        text: "测试消息之内容:很高兴见到你!祝你好运!",
        //sound:null  //add by 20160805,不发声音,或者换成自己的声音文件,不加该项就是缺省的 beep
      };

      if (sound == 0) {
        console.log("不会发出声音......");
        event.id = 1;
        event.sound = null;
      }
      else if (sound == 1) {
        console.log("会发出beep声音......");
        event.id = 2;
      }
      else if (sound == 2) {
        console.log("发出我自己的声音......");
        event.id = 3;
        event.sound = "file://audio/beng.mp3"
      }
      else {
        //do nothing
      }
      ;

      document.addEventListener("deviceready", function () {
        $cordovaLocalNotification.schedule(event).then(function () {
          console.log("local add : success 恭喜你,本地消息发送成功!");
        });

      }, false);

    };

    document.addEventListener("deviceready", function () {
      $rootScope.$on("$cordovaLocalNotification:trigger", function (event, notification, state) {
        console.log("notification id:" + notification.id + " state: " + state);
        console.log("点击消息之后会走到这里!请继续......");
      });
    }, false);
  });

