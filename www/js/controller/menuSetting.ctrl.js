angular.module('starter.menuSetting.ctrl', [])
.controller('MenuSettingCtrl', function ($scope, $stateParams, $ionicPopup,
                                         $window, CheckNavigator, $ionicHistory,
                                         localStorageService, $cordovaDatePicker) {
  $scope.checkNavigator = function () {
    var sss1 = CheckNavigator.log();
    console.log("sss1 >>>>>>>>>>>>>>" + sss1);
    var alertPopup = $ionicPopup.alert(
      {
        title: '配置清单',
        template: sss1
      }
    );
    alertPopup.then(function (res) {
      console.log("res >>>>>>>>>>>>>>" + res);
    });

  };

  $scope.clearLocalStorage = function () {
    var sss1 = "您真的要清空 本地存储 吗 ？";
    console.log("sss1 >>>>>>>>>>>>>>" + sss1);
    var confirmPopup = $ionicPopup.confirm(
      {
        title: '友情提示',
        template: sss1
      }
    );
    confirmPopup.then(function (res) {
      if (res) {
        localStorageService.clear();
        //console.log(sss1);
        var BROADCAST_NAME = 'localstorage.clear';
        $scope.$emit(BROADCAST_NAME, null);
        console.log('发送广播啦 clear localstorage >>>>>>');
      } else {
        console.log('您取消了！啥都没有做');
      }
    });
  };

  $scope.goBack = function () {
    if ($ionicHistory.backView != null) {
      $ionicHistory.goBack();
    }
  };

  $scope.datePicker = function () {
    console.log("datePickerdatePickerdatePicker >>>>>");
    if (!window.cordova) {
      var myDate = new Date();
      myDate.getYear();        //获取当前年份(2位)
      myDate.getFullYear();    //获取完整的年份(4位,1970-????)
      myDate.getMonth();       //获取当前月份(0-11,0代表1月)
      myDate.getDate();        //获取当前日(1-31)
      myDate.getDay();         //获取当前星期X(0-6,0代表星期天)
      myDate.getTime();        //获取当前时间(从1970.1.1开始的毫秒数)
      myDate.getHours();       //获取当前小时数(0-23)
      myDate.getMinutes();     //获取当前分钟数(0-59)
      myDate.getSeconds();     //获取当前秒数(0-59)
      myDate.getMilliseconds();    //获取当前毫秒数(0-999)
      myDate.toLocaleDateString();     //获取当前日期
      var mytime = myDate.toLocaleTimeString();     //获取当前时间
      myDate.toLocaleString();        //获取日期与时间
      $scope.myDate = myDate;
      console.log("date >>>>>" + myDate);
    }
    else {
      var options = {
        date: new Date(),
        mode: 'date', // or 'time'
        minDate: new Date() - 10000,
        allowOldDates: true,
        allowFutureDates: false,
        doneButtonLabel: 'DONE',
        doneButtonColor: '#F2F3F4',
        cancelButtonLabel: 'CANCEL',
        cancelButtonColor: '#000000'
      };

      document.addEventListener("deviceready", function () {

        $cordovaDatePicker.show(options).then(function (date) {
          console.log("date >>>>>" + date);
          console.log("mytime >>>>>" + mytime);

          $scope.myDate = date;
        });

      }, false);
    }

  };

});
