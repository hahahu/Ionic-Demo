angular.module('starter.toastTest.ctrl', [])

  .controller('ToastTestCtrl', function ($scope, Toast, MsgBox) {
      console.log("ToastTestCtrl >>>>>>");

      var sss = "这个功能只能在手机使用";
      if (!window.cordova) {
        MsgBox.show(sss);
        return;
      }

      $scope.showLongCenter = function (msg) {
        Toast.showLongCenter(msg);
      };

      $scope.showLongTop = function (msg) {
        Toast.showLongTop(msg);
      };

      $scope.showLongBottom = function (msg) {
        Toast.showLongBottom(msg);
      };

      $scope.showShortCenter = function (msg) {
        Toast.showShortCenter(msg);
      };

      $scope.showShortTop = function (msg) {
        Toast.showShortTop(msg);
      };

      $scope.showShortBottom = function (msg) {
        Toast.showShortBottom(msg);
      };

    }
  );
