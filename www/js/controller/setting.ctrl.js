angular.module('starter.setting.ctrl', [])
  .controller('SettingCtrl', function ($scope, $stateParams, $state,
                                       $ionicModal, $ionicPopup, $cordovaCamera,
                                       $ionicActionSheet, $http,
                                       $cordovaFileTransfer, MsgBox,
                                       $cordovaAppVersion, GetApkVersion,
                                       $timeout) {

      if (window.cordova) {
        $cordovaAppVersion.getVersionNumber().then(function (version) {
          //这是本地打包后 app 版本号,在 config.xml 配置
          $scope.appVersionNumber = version;
          console.log("appVersion 111 === " + appVersion);
        }),
          function () {
            $scope.appVersionNumber = "未可知";
          };
      }
      else {
        $scope.appVersionNumber = "未可知";
      }

      //add by wzh 20160708 检查版本升级
      $scope.checkUpdate = function () {
        //获取版本号 api :包括版本号\APK
        var apkUrl = "http://api2.dhbm.cn/wap/GetVersionAppH5/?key=3414d916c27e14bc3e9e08a30660d5ec268e76d009aee2a2c5a828973f40d223&appid=8880018&flag=com.dhbm.lmsd&encoding=utf-8&resulttype=json";

        GetApkVersion.upgradeApp(apkUrl);
      };

      $scope.gotoBarcode = function () {
        //console.log("gotoBarcode！gotoBarcode >>>>>>");
        $state.go("app.barcode");
      };

      $scope.popupBarcode = function () {
        //console.log("popupBarcode！popupBarcode >>>>>>");
        //这是二维码要用到的 text
        $scope.urlDHBM = "www.dhbm.cn";

        var templateUrl = 'templates/qrModal.html';
        $ionicModal.fromTemplateUrl(templateUrl, {
          scope: $scope
        }).then(function (modal) {
          $scope.modal = modal;
          //这里直接创建 modal，然后 show
          $scope.modal.show();
        });

        //什么时间销毁？待处理
        $scope.closeMe = function () {
          $scope.modal.hide();
        };
      };

      //add by wzh 20160219 拍照之前,选择拍照 or 从相册
      $scope.takePicture123 = function () {
        if (!window.cordova) {
          MsgBox.show("这个功能只能在手机使用.....");
          return;
        }

        var hideSheet = $ionicActionSheet.show({
          buttons: [
            {text: '现在拍照'},
            {text: '从相册选取'}
          ],
          //很简单的选择,就不用标题提示,也不用警告项目了
          //destructiveText: 'Delete',
          titleText: '请选择...',
          cancelText: '取消',
          cancel: function () {
            //取消了,do nothing
            return false;
          },
          buttonClicked: function (index) {
            console.log("index ===" + index);
            //只有2项,就不用 switch 了
            $scope.takePicture(index);
            return true;
          }
        });

        $timeout(function () {
          hideSheet();
        }, 100000);
      };

      //这个照相会将照片显示在 img 框,所以,ctrl需要在这里
      $scope.takePicture = function (mSourceType) {
        $scope.uploadFinished2 = false;
        console.log("mSourceType ===" + mSourceType);
        if (!window.cordova) {
          MsgBox.show("这个功能只能在手机使用.....");
        }
        else if (!$cordovaCamera) {
          var sss = "$cordovaCamera : 这个插件没有安装或者引入";
          MsgBox.show(sss);
        }
        else {
          if (mSourceType == 0) {
            $scope.mSourceType = Camera.PictureSourceType.CAMERA;
            $scope.mAllowEdit = true;//false 也可以
          }
          else {
            $scope.mSourceType = Camera.PictureSourceType.PHOTOLIBRARY;//SAVEDPHOTOALBUM;//
            $scope.mAllowEdit = false;//true 不可以
          }


          console.log("$scope.mSourceType ===" + $scope.mSourceType);

          var options = {
            quality: 50,
            destinationType: Camera.DestinationType.FILE_URI,//.DATA_URL,//
            sourceType: $scope.mSourceType,//Camera.PictureSourceType.SAVEDPHOTOALBUM,//.PHOTOLIBRARY, //.CAMERA,
            allowEdit: $scope.mAllowEdit,//true,//false,//
            encodingType: Camera.EncodingType.JPEG,
            targetWidth: 217,
            targetHeight: 217,
            popoverOptions: CameraPopoverOptions,
            saveToPhotoAlbum: true,//false,//
            correctOrientation: true
          };

          $cordovaCamera.getPicture(options).then(function (imageData) {
              $scope.showImgFrame = true;//只有拍照后才显示img
              //这是 返回 file
              if (options.destinationType == Camera.DestinationType.FILE_URI)
                $scope.cameraimage = imageData;
              else
              //这是base 64
                $scope.cameraimage = "data:image/jpeg;base64," + imageData;

              //这是 jq 的 dom 方法
              /*
               $scope.showImgFrame2 = true;//只在 jq 方法用的时候才显示
               var img123 = document.getElementById('myImage');
               img123.src = imageData;
               */

              //直接使用 fun 获取图片大小方法
              var imgLoad = function (url, callback) {
                var img = new Image();
                img.src = url;

                if (img.complete) {
                  callback(img.width, img.height);
                } else {
                  img.onload = function () {
                    callback(img.width, img.height);
                    img.onload = null;
                  };
                }
              };

              imgLoad(imageData, function (x, y) {
                //这个方法 就只log一下,不显示出来了
                console.log("xxx 55511 === " + x);
                console.log("yyy 55511 === " + y);
              });

              //等 load之后,这里上传 base64 数据: imageData
              if (mSourceType == 0) {
                $scope.uploadFile(imageData);
              } else {
                //从相册获取并且压缩后,文件名会加上?XXXXXXXXX
                //var imageData = "file:///storage/emulated/0/Android/data/com.dhbm.demo123/cache/Screenshot_2016-02-26-19-53-38.jpeg?1456495445491";
                var exts = ['.jpg', '.gif', '.png', '.jpeg'];
                var sss = "";
                //console.log("imageData 44444 ===" + imageData);
                for (var x in exts) {
                  if (imageData.indexOf(exts[x]) > 0) {
                    sss = imageData.split(exts[x])[0] + exts[x];
                    break;
                  }
                }
                if (sss == "")
                  sss = imageData + ".jpg";

                //如果还有错?交给api处理吧
                $scope.uploadFile(sss);
              }


            }, function (err) {
              console.log('Failed because: ');
              console.log(err);
            }
          );
        }
      };

      //这是自定义的 onload 事件
      $scope.onImgLoad = function () {
        console.log("onImgLoad onImgLoad onImgLoad 9999 === ");
        var img = new Image();
        img.src = $scope.cameraimage;

        var x = img.width;
        var y = img.height;
        var z = typeof(img.fileSize) == 'undefined' ? '不可知' : img.fileSize;//怎么不出来呢？

        $scope.width = x;
        $scope.height = y;
        $scope.size = z;
        $scope.src = img.src;

        //console.log("xxx 7777888 === " + x);
        //console.log("yyy 777888 === " + y);
      };

      //add by wzh 20160222 上传图片测试//
      //浏览器 端存在跨域 ,以后再处理,使用 open 一个新的 window,或者使用 iframe
      $scope.sendPicture = function () {
        //var popupWindow = window.open('http://192.168.1.101:8000/index.html');
        // var popupWindow = window.open('http://192.168.1.100:8000/index.html');
        var mUrl = 'http://192.168.1.199:8000/index.html';

        if (!window.cordova) {
          //这是在 pc 端运行
          window.location.href = mUrl;
        }
        else {
          //手机端
          var options = {
            location: 'yes',
            clearcache: 'yes',
            toolbar: 'no'
          };

          //'http://ngcordova.com'
          $cordovaInAppBrowser.open(mUrl, '_blank', options)
            .then(function (event) {
              // success
              console.log("$cordovaInAppBrowser success >>>>> ");
            })
            .catch(function (event) {
              // error
              console.log("$cordovaInAppBrowser error <<<<<< " + event);
            });
          $cordovaInAppBrowser.close();


          $rootScope.$on('$cordovaInAppBrowser:loadstart', function (e, event) {
            //TODO
          });

          $rootScope.$on('$cordovaInAppBrowser:loadstop', function (e, event) {
            // insert CSS via code / file
            $cordovaInAppBrowser.insertCSS({
              code: 'body {background-color:blue;}'
            });

            // insert Javascript via code / file
            $cordovaInAppBrowser.executeScript({
              file: 'script.js'
            });
          });

          $rootScope.$on('$cordovaInAppBrowser:loaderror', function (e, event) {

          });

          $rootScope.$on('$cordovaInAppBrowser:exit', function (e, event) {

          });
        }
      };


      //手机端使用 ng-cordova
      $scope.uploadFile = function (imgFile) {
        document.addEventListener('deviceready', function () {
          // Destination URL
          //var url = "http://example.gajotres.net/upload/upload.php";
          var url = "http://fangche.dhbm.cn/index.php/Api/Upload/pic_upload/?resulttype=json";
          //var url = "http://yltt.dhbm.cn/syssysmanage/index.php/Api/Upload/pic_upload_base64/?resulttype=json";

          //File for Upload
          var targetPath = imgFile;//cordova.file.externalRootDirectory + "testImage.png";

          // File name only
          var filename = targetPath.split("/").pop();

          var options = {
            fileKey: "file",
            fileName: filename,
            chunkedMode: false,
            mimeType: "image/jpg",
            params: {'directory': 'upload', 'fileName': filename} // directory represents remote directory,  fileName represents final remote file name
          };

          console.log("targetPath 22222 ===: " + targetPath);
          $cordovaFileTransfer.upload(url, targetPath, options).then(function (result) {
            console.log("SUCCESS: " + JSON.stringify(result.response));
            $scope.uploadProgress = 100;
            $timeout(function () {
              $scope.uploadFinished = true;
              $scope.uploadFinished2 = true;

              //上传完成之后,clean  // only for FILE_URI
              $cordovaCamera.cleanup().then(function () {
                  console.log("$cordovaCamera cleanup finished ! ");
                }
              );

            }, 1000);
          }, function (err) {
            console.log("ERROR: " + JSON.stringify(err));
          }, function (progress) {
            //console.log("progress.loaded: " + progress.loaded);
            //console.log("progress.total: " + progress.total);
            $timeout(function () {
              $scope.uploadProgress = parseInt((progress.loaded / progress.total) * 100);
            });
          });
        }, false);
      };

    }
  );
