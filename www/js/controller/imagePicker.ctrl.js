angular.module('starter.imagePicker.ctrl', [])
//add by wzh 20160228
.controller('ImagePicker', function ($scope, $state, $cordovaImagePicker, MsgBox) {
    /*
     //在pc 先测试
     var imageLists = new Array();
     imageLists[0] = "https://randomuser.me/api/portraits/thumb/women/55.jpg";
     //imageLists[1] = "https://randomuser.me/api/portraits/thumb/women/55.jpg";
     //imageLists[2] = "https://randomuser.me/api/portraits/thumb/women/55.jpg";
     //重复数据也会错误的 ! Duplicates in a repeater are not allowed 因为相同的内容重复引起

     $scope.imageLists = imageLists;

     console.log("imageLists >>>>>>>>>>>>>" + $scope.imageLists.length);
     */

    var sss = "这个功能只能在手机使用";
    if (!window.cordova) {
      MsgBox.show(sss);
      return;
    }

    var options = {
      maximumImagesCount: 10,
      width: 200,
      height: 200,
      quality: 80
    };

    $cordovaImagePicker.getPictures(options)
      .then(function (results) {
        for (var i = 0; i < results.length; i++) {
          console.log('Image URI: ' + results[i]);
        }
        $scope.imageLists = results;
        console.log("imageLists 222 >>>>>" + $scope.imageLists.length);
      }, function (error) {
        // error getting photos
      });

  }
);
