angular.module('starter.fileTransfer.ctrl', [])

  .controller('FileTransferCtrl', function ($scope, $rootScope, $cordovaFileTransfer, MsgBox, $timeout) {
    var sss = "<b><h2> 这个功能只能在手机使用</h2></b>";
      if (!window.cordova) {
        MsgBox.show(sss);
        return;
      }

      $scope.downloadFile = function () {
        document.addEventListener('deviceready', function () {
          var url = "http://cdn.wall-pix.net/albums/art-space/00030109.jpg";
          //var fileDir1 = cordova.file.documentsDirectory ; //这个 android 没有,返回 null
          var fileDir1 = cordova.file.externalRootDirectory;
          //var fileDir1 = cordova.file.applicationDirectory ;  //这个 只读
          //var fileDir1 = cordova.file.dataDirectory ;

          var fileName = "testImage.png";
          var fileDir = fileDir1 + fileName;

          var trustHosts = true;
          var options = {};

          console.log("dataDirectory fileDir1== " + fileDir1);
          console.log("fileDir== " + fileDir);
          var download = $cordovaFileTransfer.download(url, fileDir, options, trustHosts).then(function (success) {
            console.log("success " + JSON.stringify(success));
            $scope.downloadProgress = 100;
            $timeout(function () {
              $scope.downloadFinished = true;
            }, 2000);
          }, function (error) {
            console.log("Error " + JSON.stringify(error));
          }, function (progress) {
            $timeout(function () {
              $scope.downloadProgress = parseInt((progress.loaded / progress.total) * 100);
            });
          });


          //不能取消,提示错误! es5.1了,也不对!是不是es6才有? <Promise> has no method 'abort'
          if ($scope.downloadProgress > 0.1) {
            console.error("abort============")
            download.abort();
          }
        })
      };

      $scope.uploadFile = function () {
        document.addEventListener('deviceready', function () {
          // Destination URL
          //var url = "http://example.gajotres.net/upload/upload.php";
          var url = "http://fangche.dhbm.cn/index.php/Api/Upload/pic_upload/?resulttype=json";
          //var url = "http://yltt.dhbm.cn/syssysmanage/index.php/Api/Upload/pic_upload_base64/?resulttype=json";

          //File for Upload
          var targetPath = cordova.file.externalRootDirectory + "testImage.png";

          // File name only
          var filename = targetPath.split("/").pop();

          var options = {
            fileKey: "file",
            fileName: filename,
            chunkedMode: false,
            mimeType: "image/jpg",
            params: {'directory': 'upload', 'fileName': filename} // directory represents remote directory,  fileName represents final remote file name
          };

          console.log("targetPath ===: " + targetPath);
          $cordovaFileTransfer.upload(url, targetPath, options).then(function (result) {
            console.log("SUCCESS: " + JSON.stringify(result.response));
            $timeout(function () {
              $scope.uploadProgress = 100;
              $timeout(function () {
                $scope.uploadFinished = true;
              }, 2000);
            }, 1000);
          }, function (err) {
            console.log("ERROR: " + JSON.stringify(err));
          }, function (progress) {
            // PROGRESS HANDLING GOES HERE
            //console.log("progress.loaded: " + progress.loaded);
            //console.log("progress.total: " + progress.total);
            $timeout(function () {
              $scope.uploadProgress = parseInt((progress.loaded / progress.total) * 100);
            });
          });
        }, false);

      };

    }
  );
