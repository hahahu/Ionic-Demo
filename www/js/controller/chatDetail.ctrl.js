angular.module('starter.chatDetail.ctrl', [])
  .controller('ChatDetailCtrl', ['$scope', '$stateParams',
    'messageService',
    '$ionicScrollDelegate',
    'socket', 'randomColor','userService',
    'Globals',
    '$timeout',
    function ($scope, $stateParams, messageService,
              $ionicScrollDelegate,
              socket, randomColor,userService,
              Globals,
              $timeout) {
      var viewScroll = $ionicScrollDelegate.$getByHandle('chatDetailsScroll');

      console.log(" chatDetailCtrl chatDetailCtrl ====");

      // $scope.hasLogined = false;
      $scope.receiver = "";//默认是群聊
      $scope.publicMessages = [];//群聊消息
      $scope.privateMessages = {};//私信消息
      $scope.messages = $scope.publicMessages;//默认显示群聊
      $scope.users = [];//
      $scope.color = randomColor.newColor();//当前用户头像颜色

      $scope.sendMessage = function (send_content) {
        console.log(" 发送啦  >>> " + send_content);
        $scope.words = send_content;
        $scope.nickname = 'sss';

        var msg = {text: $scope.words, type: "normal", color: $scope.color, from: $scope.nickname, to: $scope.receiver};

        var rec = $scope.receiver;
        if (rec) {  //私信
          if (!$scope.privateMessages[rec]) {
            $scope.privateMessages[rec] = [];
          }
          $scope.privateMessages[rec].push(msg);
        } else { //群聊
          $scope.publicMessages.push(msg);
        }
        $scope.words = "";//清空底部输入的内容？是不是应该放在emit之后？待处理

        if (rec !== $scope.nickname) { //排除给自己发的情况
          //自己发给自己只在本地显示，不要发送到服务端，当然如果系统要求，可以放开
          socket.emit("addMessage", msg);
        }

        //add by wzh 20160420 直接刷新,并写入 LS
        //发送消息没有发送结果返回?应该在收到发送结果之后吧？
        // 或者应该有个状态变化:发送中,已发送
        //这个待处理
        $scope.doRefresh();
      };

      //改成以上 sendMessage ,待删除
      $scope.postMessage = function () {
        // $scope.words = "哈哈哈哈哈哈12345";

        var msg = {text: $scope.words, type: "normal", color: $scope.color, from: $scope.nickname, to: $scope.receiver};
        var rec = $scope.receiver;
        if (rec) {  //私信
          if (!$scope.privateMessages[rec]) {
            $scope.privateMessages[rec] = [];
          }
          $scope.privateMessages[rec].push(msg);
        } else { //群聊
          $scope.publicMessages.push(msg);
        }
        $scope.words = "";//清空底部输入的内容？是不是应该放在emit之后？待处理
        if (rec !== $scope.nickname) { //排除给自己发的情况
          //自己发给自己只在本地显示，不要发送到服务端，当然如果系统要求，可以放开
          socket.emit("addMessage", msg);
        }
      }

      //接收到新消息
      socket.on('messageAdded', function (data) {
        console.log(data);
        console.log("收到消息 data.from <<< " + data.from);
        console.log("收到消息 data.to <<< " + data.to);

        if (!$scope.hasLogined) return;

        if (data.to) { //私信
          if (!$scope.privateMessages[data.from]) {
            $scope.privateMessages[data.from] = [];
          }
          $scope.privateMessages[data.from].push(data);
        } else {//群发
          $scope.publicMessages.push(data);
        }

        var fromUser = userService.get($scope.users, data.from);
        var toUser = userService.get($scope.users, data.to);
        if ($scope.receiver !== data.to) {//与来信方不是正在聊天当中才提示新消息
          if (fromUser && toUser.nickname) {
            fromUser.hasNewMessage = true;//私信
          } else {
            toUser.hasNewMessage = true;//群发
          }
        }

        //add by wzh 20160420 直接刷新,并写入 LS
        $scope.doRefresh();
      });


      $scope.doRefresh = function () {
        console.log("doRefresh ok");
        $scope.messageNum += 5;// ?一次增加 5
        $timeout(function () {
          $scope.messageDetils = messageService.getAmountMessageById($scope.messageNum,
            $stateParams.messageId);
          $scope.$broadcast('scroll.refreshComplete');
        }, 200);
      };


      $scope.$on("$ionicView.beforeEnter", function () {
        //这个部分功能是不是该转移到 chatRoot?待处理
        //全局变量 mLoginData 会在app run的时候初始化
        var loginData = Globals.mLoginData;
        console.log("chatCtrl >>>>>>" + loginData);

        $scope.nickname = loginData.username;
        $scope.hasLogined = loginData.hasLogined;

        if (!loginData.hasLogined) {
          var sss = "<b><h2> 请先登录...</h2></b>";
          MsgBox.show(sss);
          return;
        }
        else {
          console.log("addUser >>>>>>" + $scope.nickname);
          socket.emit("addUser", {nickname: $scope.nickname, color: $scope.color});
        }


        $scope.message = messageService.getMessageById($stateParams.messageId);
        $scope.message.noReadMessages = 0;
        $scope.message.showHints = false;
        messageService.updateMessage($scope.message);
        $scope.messageNum = 10;
        $scope.messageDetils = messageService.getAmountMessageById($scope.messageNum,
          $stateParams.messageId);
        $timeout(function () {
          viewScroll.scrollBottom();
        }, 0);
      });

      //这个真的需要吗?我已经在 app设置了,待处理
      window.addEventListener("native.keyboardshow", function (e) {
        viewScroll.scrollBottom();
      });
    }
  ])
