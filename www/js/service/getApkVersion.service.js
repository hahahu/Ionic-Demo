angular.module('starter.services.getApkVersion', [])
  //获取网上安装版的 apk 版本号
  .factory('GetApkVersion', GetApkVersion);

//注意以下次序必须完全对上
GetApkVersion.$inject = [
  '$http', '$cordovaAppVersion',
  '$ionicPopup', '$ionicLoading',
  '$cordovaFile', '$cordovaFileOpener2',
  '$cordovaFileTransfer', 'MsgBox',
  '$cordovaNetwork', '$timeout'
];

function GetApkVersion($http, $cordovaAppVersion,
                       $ionicPopup, $ionicLoading,
                       $cordovaFile, $cordovaFileOpener2,
                       $cordovaFileTransfer, MsgBox,
                       $cordovaNetwork, $timeout) {
  return {
    about: function (apkUrl) {
      $cordovaAppVersion.getVersionNumber().then(function (version) {
        var appVersion = version;

        var sss = "<h4>当前版本:" + appVersion + "</h4>";
        //var s1 = '<button class="button  button-balanced"ng-click="GetApkVersion.upgradeApp(apkUrl)">检查新版本</button>';
        var s1 = '<button class="button  button-balanced"ng-click="GetApkVersion.upgradeApp(apkUrl)">检查新版本</button>';

        sss = sss + s1;

        var alertPopup2 = $ionicPopup.alert(
          {
            //scope: $scope,
            title: "版本信息",
            //template: sss,
            templateUrl: './templates/about.html'
          }
        );
        alertPopup2.then(function (res) {
          console.log("res >>>>>>>>>>>>>>" + res);
        });
      });
    },

    upgradeApp: function (apkUrl) {
      //var apkUrl = "http://api2.dhbm.cn/wap/GetVersionAppH5/?key=3414d916c27e14bc3e9e08a30660d5ec268e76d009aee2a2c5a828973f40d223&appid=8880018&flag=com.dhbm.lmsd&encoding=utf-8&resulttype=json";

      if (apkUrl == null) return null;

      var apkVersion = "";
      var apkNameUrl = "";
      var apkName = "";
      var appVersion = "未可知";

      //首先获取网上 apk 版本号
      $http.get(apkUrl)
        .then(function (response) {
          //console.log("response === ");
          //console.log(response);
          apkVersion = response.data.version + "." + response.data.sub;
          apkNameUrl = response.data.apk;
          //console.log("apkName 222 === " + apkName);
          console.log("apkVersion 222 === " + apkVersion);
          //获取url最后一部分,作为apk 名字
          var x = apkNameUrl.split('/');
          var l = x.length;

          if (l < 1) {

            var alertPopup1 = $ionicPopup.alert(
              {
                title: "错误提示",
                template: "api 返回结果异常,请检查!" + apkNameUrl
              }
            );

            alertPopup1.then(function (res) {
              console.error("api 返回结果异常,请检查!" + apkNameUrl);
            });

            return;
          }

          apkName = x[l - 1];
          //只有手机上才会继续比较版本号,升级
          // pc上只会执行前面一部分,方便在电脑调试
          if (window.cordova) {
            $cordovaAppVersion.getVersionNumber().then(function (version) {
              //这是本地打包后 app 版本号,在 config.xml 配置
              appVersion = version;
              console.log("appVersion 111 === " + appVersion);

              //比较版本号,简单处理:不比较大小,只要不一样,就升级
              if (appVersion < apkVersion) {
                showUpdateConfirm(apkNameUrl, apkName, appVersion, apkVersion);
              }
              //没有新版,弹出提示
              else {
                /*
                 var alertPopup2 = $ionicPopup.alert(
                 {
                 title: "版本信息",
                 template: "<h4> 当前版本 < " + appVersion + "></h4>"
                 }
                 );
                 alertPopup2.then(function (res) {
                 console.log("res >>>>>>>>>>>>>>" + res);
                 });
                 */
              }
            });
          }
          else {
            var sss = "<h4> 当前版本 < " + appVersion + "></h4>";
            sss = sss + "<h4> 网上apk版本 < " + apkVersion + "></h4>";
            var alertPopup2 = $ionicPopup.alert(
              {
                title: "版本信息",
                template: sss
              }
            );
            alertPopup2.then(function (res) {
              console.log("res >>>>>>>>>>>>>>" + res);
            });
          }

        });

    }

    //以上 方法 (method) 为对外提供的服务
    //==========================
  };

  //-------------------------------
  //以下函数供调用
  // 显示是否更新对话框
  function showUpdateConfirm(apkNameUrl, apkName, appVersion, apkVersion) {
    var newtork = $cordovaNetwork.getNetwork();
    var sss = '<h4>是否升级到版本:' + apkVersion + "?</h4>";

    if (newtork !== 'wifi' && newtork !== 'Connection.WIFI') {
      sss = sss + '<h4>当前使用的网络不是 Wi-Fi，将会产生网络流量</h4>';
    }

    var confirmPopup = $ionicPopup.confirm({
      title: '升级提示',
      template: sss,
      cancelText: '以后再说',
      okText: '升级'
    });

    confirmPopup.then(function (res) {
      if (res) {
        var u = navigator.userAgent;
        var isiOS = !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/); //ios终端
        console.log("这是 ios 吗? =====" + isiOS);
        //alert('是否是iOS：'+isiOS);
        if (isiOS) {
          //还有这招啊?测试结果:可行!不装 inappbrowser 也可以啊
          //调用浏览器打开下载链接，需要安装 inappbrowser 插件
          //我们的api 没有返回 ios 的 下载地址,不同的 app自行手工修改
          var url = "m.dhbm.cn/app/qy/lmsd/ios/"; //可以从服务端获取更新APP的路径
          window.open(url, '_system', 'location=yes');
        }
        else
          downLoadApk(apkNameUrl, apkName);
      } else {
        // 取消更新
        console.log("取消更新 选择 =====" + url);
      }
    });

  }

  //
  function downLoadApk(apkNameUrl, apkName) {
    //ionic 的 loading 总是不舒服,换成一个dialog
    //首先放置一个 loading
    var mTitle = "升级提示";
    var mLoadingContent = "正在下载中...";
    //add by wzh 20160802 允许取消下载
    errorFn = function(){
      console.error("errorFn errorFn ====");
      download.abort();
    };
    cancelFn = function(){
      console.log("cancelFn cancelFn ====");
      //不能取消,提示错误! es5.1了,也不对!是不是es6才有? <Promise> has no method 'abort'
      //download.abort();
    };
    cancelable = true;;
    cancelableText = '使用后台升级';


    var progressLoading = window.navigator.dialogsPlus.progressStart(mTitle, mLoadingContent,
      errorFn,cancelable,cancelableText,cancelFn);
    //progressLoading.setValue(0);

    var url = apkNameUrl;//"http://m.dhbm.cn/app/qy/lmsd/an/dhbm.lmsd.1.0.160707.apk"
    var targetPath = "file:///storage/sdcard0/Download/" + apkName;
    //APP下载存放的路径，可以使用cordova file插件进行相关配置
    var trustHosts = true;
    var options = {};
    var downloadProgress = 0;
    var downloadFinished = false;

    var download = $cordovaFileTransfer.download(url, targetPath, options, trustHosts).then(function (result) {
      downloadProgress = 100;
      console.log("downloadProgress 完成====" + downloadProgress);
      progressLoading.hide();
      $timeout(openAndInstallApk(targetPath), 2000);

    }, function (error) {
      //by wzh 20160801 错了？先取消 dialog
      console.error("Error " + JSON.stringify(error));
      progressLoading.hide();
    }, function (progress) {
      downloadProgress = parseInt((progress.loaded / progress.total) * 100);
      //console.log("downloadProgress ====" + downloadProgress);
      $timeout(function () {
        progressLoading.setValue(downloadProgress);
      });

    });

  }

  //打开 apk,系统会自动执行安装
  function openAndInstallApk(targetPath) {
    $cordovaFileOpener2.open(targetPath, 'application/vnd.android.package-archive'
    ).then(function () {
      // 成功
      console.log("open 成功=== " + targetPath);
    }, function (err) {
      // 错误
      var alertPopup = $ionicPopup.alert(
        {
          title: "错误提示",
          template: "非法的安装包,请检查!" + err
        }
      );
      alertPopup.then(function (err) {
        console.log("非法的安装包,请检查!" + err);
      });
    });
  };

}
;


