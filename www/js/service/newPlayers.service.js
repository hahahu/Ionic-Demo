angular.module('starter.services.newPlayers', [])
  .factory('NewPlayers', function (Globals, $http) {
    // 这是一个专门提供了一个测试api !真的很不错啊 http://api.randomuser.me/?results=10
    var API_URL = "http://api.randomuser.me/";
    var HTTP_TIMEOUT = 20000;

    //console.log("services NewPlayers === NewPlayers ======== ");
    return {
      getFeedPlayers: function () {
        //getFeedPlayers 本身不带参数，获取返回值的函数带参数
        return $http.get(API_URL + '?results=10', {timeout: HTTP_TIMEOUT})
          .then(function (response) {
              console.log(response.data.results);
              return response.data.results;
            }
            , function (response) {
              //带有错误信息的resp,只要是错误,就直接返回 null
              console.log("错误 111 ");
              console.log(response);
              return null;
            }
          );
      },
      getNewPlayers: function () {
        return $http.get(API_URL + '?results=10', {timeout: HTTP_TIMEOUT}).then(function (response) {
            console.log(response.data.results);
            return response.data.results;
          }
          , function (response) {
            //带有错误信息的resp,只要是错误,就直接返回 null
            console.log("错误 222 ");
            console.log(response);
            return null;
          }
        );
      },

      //add by wzh 20150110 获取单个 player 数据 ，按照 playlist.user.salt
      getThisPlayer: function (salt) {
       //by wzh 20160731 先去掉全局Globals,待处理
        console.log("getThisPlayer 222222222222222 ");
        /*
        for (var i = 0; i < Globals.mPlayLists.length; i++) {
          if (Globals.mPlayLists[i].login.salt === salt) {
            return Globals.mPlayLists[i];
          }
        }
        */
        return null;
      }

    };
  });

// 以下是一个 http 返回的样本
//到底哪一个字段是唯一的？ salt 试试
var test1 =
{
  "results": [
    {
      "user": {
        "gender": "female",
        "name": {
          "title": "miss",
          "first": "mary",
          "last": "brooks"
        },
        "location": {
          "street": "8917 pearse street",
          "city": "Carrigtwohill",
          "state": "texas",
          "zip": 83019
        },
        "email": "mary.brooks@example.com",
        "username": "tinysnake378",
        "password": "janelle",
        "salt": "mvJU7m3E",
        "md5": "7abce82c676ce3dcf3451bfd8b994b5f",
        "sha1": "38504e7d4a0d10305a8bebd8c63f3ffa11a98d08",
        "sha256": "005db958b77395bd9047f3c95e3d998cc3f61155cf7ecbacf082bf7158de73b6",
        "registered": 1105765217,
        "dob": 1346807123,
        "phone": "041-082-8087",
        "cell": "081-654-2131",
        "PPS": "3095343T",
        "picture": {
          "large": "https://randomuser.me/api/portraits/women/55.jpg",
          "medium": "https://randomuser.me/api/portraits/med/women/55.jpg",
          "thumbnail": "https://randomuser.me/api/portraits/thumb/women/55.jpg"
        }
      }
    }
  ],
  "nationality": "IE",
  "seed": "7aa4877275eb7c5705",
  "version": "0.7"
};


// by wzh 20160413 这个 api 数据格式改变了,以下是样本,去掉注释看的清楚一些
var test2 =
{
  "results": [{
    "gender": "male",
    "name": {
      "title": "mr",
      "first": "benedikt",
      "last": "henkel"
    },
    "location": {
      "street": "9804 raiffeisenstraße",
      "city": "hagen",
      "state": "sachsen",
      "postcode": 88185
    },
    "email": "benedikt.henkel@example.com",
    "login": {
      "username": "ticklishpeacock719",
      "password": "tyvugq",
      "salt": "9ebN8USc",
      "md5": "21ac48004f07d88bfe08625a3c3fc1fb",
      "sha1": "69e4e55e28e00fa82daaabfbdc78a0c6f3aead3d",
      "sha256": "677bf495cfc2464edbb271576f0837bbd28408fedabdedfcf68144cba4226cbe"
    },
    "registered": 1414929752,
    "dob": 707731125,
    "phone": "0302-4479177",
    "cell": "0171-9721059",
    "id": {
      "name": "",
      "value": null
    },
    "picture": {
      "large": "https://randomuser.me/api/portraits/men/58.jpg",
      "medium": "https://randomuser.me/api/portraits/med/men/58.jpg",
      "thumbnail": "https://randomuser.me/api/portraits/thumb/men/58.jpg"
    },
    "nat": "DE"
  }], "info": {"seed": "e2e5fdf51de37c57", "results": 1, "page": 1, "version": "1.0"}
};

