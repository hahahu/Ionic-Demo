angular.module('starter.services.socket', [])
  // 这个服务负责和服务器socket通信处理
  /*包括
   1 与服务器:连接 重连
   2 发送给服务器 登录\退出 请求
   3 接收服务器返回的 连接\登录 成功和失败结果
   4 发送我自己的聊天内容给服务器
   4 接收服务器转来的聊天内容
   */

  .factory('socket', function ($rootScope) {
    //by wzh 2015117 client是在这里 connect 到 server的 ？
    //都不需要 this.socket = io.connect('ws://127.0.0.1:3000');
    //var socket = io(); //默认连接部署网站的服务器
    //by wzh 20160413 加上具体 url
    var socket = io.connect('ws://127.0.0.1:3002');
    return {
      on: function (eventName, callback) {
        socket.on(eventName, function () {
          console.log("收到服务器事件 <<< " + eventName);
          var args = arguments;
          $rootScope.$apply(function () {   //手动执行脏检查
            callback.apply(socket, args);
          });
        });
      },
      emit: function (eventName, data, callback) {
        socket.emit(eventName, data, function () {
          console.log("发送给服务器事件 eventName >>> " + eventName);
          console.log("发送给服务器事件 data >>> " + data);
          var args = arguments;
          $rootScope.$apply(function () {
            if (callback) {
              callback.apply(socket, args);
            }
          });
        });
      }
    };
  });
