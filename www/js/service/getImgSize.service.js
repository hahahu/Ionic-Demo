angular.module('starter.services.getImgSize', [])
.factory("GetImgSize", function ($q) {
  return {
    show: function (url) {
      var defer = $q.defer;

      var img = new Image();
      img.src = url;

      if (img.complete) {
        //callback(img.width, img.height);
        defer.resolve(img);
      } else {
        img.onload = function () {
          //callback(img.width, img.height);
          defer.resolve(img);
          img.onload = null;
        };
      }

      return defer.promise;
    }
  }

});
