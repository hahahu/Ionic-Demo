angular.module('starter.services', [])
  //使用一个服务来共享本 app 的公共变量
  .factory('Globals', function () {
    return {
      mPlayLists: {},
      mLoginData: {},
      //add by wzh 20160719 保存app信息
      mAppVersion:{}
    }
  })

  //by wzh 20160315 待解决绑定到 rootscope 之后再处理
  .factory("ReconnectNetWork", function ($cordovaToast) {
    return {
      show: function (message) {
      }
    }
  })

  //by wzh 20160418 伪造聊天数据
  .factory('NewFriends', function (Globals,localStorageService, $http) {
    // 这是一个专门提供了一个测试api !真的很不错啊 http://api.randomuser.me/?results=10
    var USER_API_URL = "data/friends.json";
    var HTTP_TIMEOUT = 20000;

    //console.log("services NewFriends === NewFriends ======== ");
    return {

    getAll: function () {
      var x = localStorageService.get("messageID");
      if(x == null){
        return $http.get(USER_API_URL, {timeout: HTTP_TIMEOUT}).then(function (response) {
          var s = response.data.results;
          console.log(s);
          //同时写入 LS,下次就有了
          localStorageService.set("messageID",s)
          return s;
        });
      }
      else return x;
      },

      getByName: function () {
        //do nothing .待增加
        return true;
      },

      clear: function () {
        //do nothing .待增加
      }

    };
  })




;
