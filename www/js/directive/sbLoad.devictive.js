angular.module('starter.directives.sbLoad', [])
.directive('sbLoad', ['$parse', function ($parse) {
  //console.log("555555555555 === " );
  return {
    restrict: 'A',
    link: function (scope, elem, attrs) {
      //console.log("999999999999999999=== " );
      var fn = $parse(attrs.sbLoad);
      elem.on('load', function (event) {
        scope.$apply(function() {
          fn(scope, { $event: event });
        });
      });
    }
  };
}]);
