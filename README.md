## 20160304 Web发布
    1 查询支持的平台是否包括浏览器
      sudo cordova platform list
    2 增加 browser 平台
      sudo ionic platform add browser
      或 sudo cordova platform add browser
    3 预编译,没有发现什么变化
         sudo ionic prepare browser
    4 编译,
       sudo ionic compile browser
      结果生成了一个 platforms/browser/build/package.zip
      但是,解压错误!"无此文件或目录",以后再处理
    5 查看所有使用的插件
      sudo cordova plugin ls   
      
## 20160302 联机测试
    1 如果本次有修改,先执行
      sudo ionic prepare android
      然后运行
      android:
        sudo ionic run --device android
      苹果(需要安装 ios-deploy)
        sudo ionic run --device ios
      
## 20160302 查看手机Logcat
    1 单独使用 Android device monitor,因为Webstorm 没有集成进去
    2 位置: 找到 android SDK 安装目录,在tools目录下的 monitor就是!
    *** 例如:我的位置在这里 /Users/dhbm/Downloads/android-sdk_r24.0.1-macosx/android-sdk-macosx/tools
    
## 20160116 App应用名字+应用图标+启动画的修改
    *** 不要手工修改 platforms 目录下的内容，那些都是自动生成的
    *** 除了这个之外： \platforms\android”下面新建一个release-signing.properties
          内容如下
          *****
          key.store=/Users/dhbm/DeskTop/Android/dhbm.keystore
          key.alias=dhbm.keystore
          key.alias.password=www.dhbm.cn
          key.store.password=www.dhbm.cn
          *****
          cordova build android --release
          生成的 apk位置：
          /Users/dhbm/Desktop/pg2015122401/platforms/android/build/outputs/apk/android-release.apk
    1、参考 http://jingyan.baidu.com/article/c35dbcb0ec59438916fcbcaf.html
    ### 修改config.xml(假设创建项目时，没有加上包名等参数)
      包名：widget id="com.dhbm.demo123"
      版本号：version="0.0.1"
      程序名：<name>IonicDemo</name>
      程序简介：<description> IonicDemo by wzh 20160116 </description>
    2、怎么直接设置编译出来的 apk 文件名？（不是上面说的 程序名、app名）待处理
      cordova compile  -help 没有发现有这个选项，就只好每次都 rename，当然只在 release 的时候才真的需要
    3、程序图标：resources 目录下 android 目录 icon 目录
    4、启动画面 splash : resources 目录下 android 目录 splash 目录
    
## 20160116 android打包测试--debug
    1、增加 platforms,预编译、编译
          ionic platforms add android
          sudo ionic prepare android
          sudo ionic compile android
    2、运行结果，列表数据不出来，说明无法访问外网(404)
    3、增加白名单
    cordova plugin add cordova-plugin-whitelist
    ionic plugin add cordova-plugin-whitelist
    4、在 index.html 首页增加 meta项， 设置安全性
    <meta http-equiv="Content-Security-Policy"
          content="script-src * ‘unsafe-eval‘; connect-src * ‘unsafe-eval‘; 
    object-src ‘self‘; style-src * ‘unsafe-inline‘; img-src *" >
      以上 meta 加上之后会报错，难道我copy错了？但是测试发现，不用加这个meta
      
    5、ion-infinite-scroll有bug，
      将services中，首次读取数据改成15条，保证超出一屏所能显示的条数
      将每次上拉刷新的条数也改成10条
    6、修改过 html之后，不用重新增加 platforms，直接预编译、编译
    7、列表ok，点击详细ok，幻灯ok！
    8、修改 sliders.html 之后，预编译发现，platforms 中对应的  sliders.html 也变化了！
      原来预编译干这个的！
    9、测试jQuery 页面，那个WX年代就停留在 0%，为什么呢？
    
    
## 20151230 tabs + sidemenu 的
### 这是一个 blank 空白项目
    创建方式 $ ionic start ion20151230 blank
### 对比tabs,sidemenu项目，区别如下：
    1、www目录没有创建 template 模板目录
    2、主页 index.html 的body 部分只有一个简单的 panel，没有 ion-nav-view 的UI-Router
    3、主程序 app.js 中，只有app的controller(这里叫做starter),并且没有任何注入参数
        ----因此，这里没有 controller.js,来扩展 starter.controllers
        ----没有初始化的残路，例如：sidemenu项目中的 playlists 和 loginData 以及 login 对应的 $ionicModal
        ----主页UI没有任何 href或者button等，也就不需要定义那些对象的controller，例如：tabs项目中的 ChatsCtrl、ChatDetailCtrl
    4、没有状态机路由 $stateProvider
    5、没有 url 路由 $urlRouterProvider，所以主页就是 index.html里面的那个 panel，不会调准到某个模板去
    6、没有自定义任何指令，例如：tabs中的sevices里面的factory方式的 chats

## 20151224 创建应用程序 
    1、我这里直接使用Webstrom创建了
    2、CLI方法 cordova create hello com.example.hello HelloWorld 
  
# 必须预先进行的安装
    1、安装 nodejs 
    2、安装JAVA
    3、安装 android sdk tools
    4、安装ANT
      这个不需要了！
    5、安装 Xcode
    
## 查看 node 安装路径    
    npm root
## 升级到稳定版本   
    n stable   
## 查看 node 版本
    node -v
## 查看 npm 版本
    npm -v  
## 查看 ionic 版本
    ionic -v
## 查看 cordova 版本  
    cordova -v

n stable

sudo npm update -g npm


## 升级 ionic
sudo npm update ionic -g

sudo npm update -g 

## 安装\升级 ionic
    sudo npm install -g ionic 

# 将整个目录权限都改成 可读写（mac必须root权限）

## 在项目目录下运行 操作 
### 安装package.json中的依赖的js模块
    sudo npm install
 
## 查看支持的平台
    sudo cordova platform list
    我的电脑结果: 
    Available platforms: amazon-fireos, blackberry10, browser, firefoxos, osx, webos

# 增加运行平台
    ionic platform add android
    ionic platform add ios
    ** githubs 需要翻墙
    ** 有时需要2-3次才能完成
    Web发布
      sudo ionic platform add browser
      或 sudo cordova platform add browser
      
      
## 编译应用程序 ：如果build一步不能正确，那么分两步完成 (直接使用 build总是不能完整)
   1、 ionic build android
    等效于：
      sudo ionic prepare android
      sudo ionic compile android
    
      cordova prepare android --release
      cordova compile android --release
    2、ionic build ios
    等效于：
      cordova prepare ios
      cordova compile ios
 
## 发布 browser 平台
       sudo ionic platform add browser
       或 sudo cordova platform add browser
     
       sudo ionic prepare browser
    
       sudo ionic compile browser
        
## www运行
    直接打开 index.html，在浏览器运行

## android 打包、运行
    *** 不要修改platforms下面的www目录，自动生成的
    *** 再次将整个目录权限都改成 可读写（mac必须root权限），因为这次又自动生成了android目录
    
    ### 使用以上增加平台、编译生成
      主文件在 ：MainActivity.java
      debug apk文件在 ../platforms/android/build/outputs/apk/android-debug.apk
      未签名 apk 位置
      ../platforms/android/build/outputs/apk/android-release-unsigned.apk
    参考 http://rensanning.iteye.com/blog/2030516
  
    ### 最终方案 ： \platforms\android”下面新建一个release-signing.properties
      内容如下
      *****
      key.store=/Users/dhbm/DeskTop/Android/dhbm.keystore
      key.alias=dhbm.keystore
      key.alias.password=www.dhbm.cn
      key.store.password=www.dhbm.cn
      *****
      cordova build android --release
      生成的 apk位置：
      /Users/dhbm/Desktop/pg2015122401/platforms/android/build/outputs/apk/android-release.apk

    参考 http://www.tuicool.com/articles/eEj2Q3
    Proguard 会混淆cordova及其插件的java代码，导致apk运行时报 cordova error initial class，
    解决办法是在proguard-project.txt 加入下面的内容，不混淆cordova及其插件
    -keep class org.apache.cordova.** { *; }
    -keep public class * extends org.apache.cordova.CordovaPlugin
    
    参考 http://lzw.me/a/cordova-3-5-android-apk-signed.html
    文件 ant.properties 怎么不行？待处理
  
## iOS 打包 发布
    *** 不要修改platforms下面的www目录，自动生成的
    *** 最好将整个目录权限都改成 可读写（mac必须root权限）
    
    使用以上增加平台
    直接生成ipa以及导入到xcode，待处理
  
## 关于windows版本的WS
    *** 不会自动产生 platforms 目录，请在terminal下cli方式增加
    ### 增加3种平台方法
      sudo ionic platforms add android
      sudo ionic platforms add ios
      sudo ionic platforms add browser


